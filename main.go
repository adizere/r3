package main

import (
	"time"
)

func main() {
	// Initiate a new node
	localNode := NewNode()

	// And start
	localNode.Start() // blocks

	for {
		select {
		case <-time.After(500 * time.Millisecond):
			if (localNode.Conf.Broadcast == true) {
				localNode.BroadcastDummyMessages(1)

				localNode.Conf.Broadcast = false
			} else {
				// sitting duck
			}
		}
	}

	// var i int = 0
	// var l int = 10
	//
	// for {
	//     select {
	//     case <- time.After(500 * time.Millisecond):
	//         if (localNode.Conf.Broadcast == true) && (i <= l) {
	//
	//             localNode.BroadcastDummyMessages(2048)
	//             i++
	//             // localNode.Conf.Broadcast = false
	//         } else {
	//         }
	//     }
	// }
}
