package main

import (
	"github.com/golang/glog"
)

// Various helper functions for creating and interpreting protocol messages

func getDataMsg(tid string, tentry []byte) *Data {
	return &Data{
		Id:    tid,
		Entry: tentry,
	}
}

func getSequenceMsgForData(did string, tid string, tsn uint64,
	data []byte, cn int32) *Sequence {

	var t = data
	t = append(t, []byte(did)...)
	t = append(t, byte(tsn))
	t = append(t,byte(cn))

	return getSequenceMsg(did, tid, tsn, hash(t), cn)
}

// TODO: assign the hash
func getSequenceMsg(did string, tid string, tsn uint64, hash []byte, cn int32) *Sequence {

	return &Sequence{
		Did:  did,
		Id:   tid,
		Sn:   tsn,
		Hash: hash,
		Cn:   cn,
	}
}

func getActivateMsg(selfId int, stop bool, n uint32) *Msg {
	var m Msg
	m.Nonce = n
	m.Type = &Msg_Activate{&Activate{
		Node: int32(selfId),
		Stop: stop,
	}}

	return &m
}

func assemblePushMsg(d []*Data, s []*Sequence, n uint32) *Msg {
	var m Msg
	m.Nonce = n
	m.Type = &Msg_Push{_assemblePush(d, s)}

	return &m
}

func _assemblePush(d []*Data, s []*Sequence) *Push {
	var p Push
	if d != nil {
		p.Data = d
	}
	if s != nil {
		p.Sequence = s
	}

	glog.Infof("Assembled a Push message with: %v and %v", len(p.Data),
		len(p.Sequence))
	return &p
}

// Returns an approximation on the size of the message in bytes
func MsgLength(m *Msg) uint64 {
	if p := m.GetPush(); p != nil {
		var tLen uint64 = uint64(len(p.GetData())) * 260
		tLen += uint64(len(p.GetSequence())) * 20

		return tLen
	}

	return 0
}

