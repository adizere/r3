package main

import (
	"strings"
	"strconv"
	"github.com/golang/glog"
)

type SuccessorRef struct {
	Position int
	Address  string
}

// Given a successor's address, of the form "id,IP:port", this function returns
// the corresponding SuccessorRef
func buildSuccessor(succRaw string) *SuccessorRef {
	s := strings.SplitN(succRaw, ",", 2)

	if len(s) != 2 {
		glog.Fatalf("Couldn't parse the successor string! (%v)", succRaw)
	}

	pos, err := strconv.Atoi(s[0])
	if err != nil {
		glog.Fatalf("Error converting to int! (%v)", s[0])
		return nil
	} else {
		return &SuccessorRef{
			Position: pos,
			Address:  s[1],
		}
	}
}
