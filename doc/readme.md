

We can run an instance of the system without actually installing golang:

```bash
docker-machine start
eval $(docker-machine env)
```

Now we're connected to the docker daemon.
Next, let's go to the directory containing the source code and from there on let docker do its magic.

```bash
cd r3/
docker build -t r3 .
docker run r3
```

To build locally (without docker):

```bash
cd r3
go build -o r3
```


# Generating the protobuf sources for Go

Using docker:

```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) znly/protoc --go_out=plugins=grpc:. -I. messages.proto
```

If you have `protoc` installed, it should be version 3, then you can do this:

```bash
protoc --go_out=plugins=grpc:. *.proto
```
