package main

import (
	"os"
	"flag"
	"encoding/json"
	"io/ioutil"
	"github.com/golang/glog"
)

const (
	//
	// Networking
	// By default, we listen on all interfaces, port 55467
	defaultListen = ":55467"
	// Client-side API – listen on HTTP 8080
	defaultClientHTTPListen = ":8080"
	// Number of parallel connections for pushing data to our successor
	pushPipelineWidth = 1
	// Initial capacity for the inbound and outbound channels
	netChansCap = 256
	// Periodicity (in seconds) of status messages
	statusIV = 10

	//
	// Log
	// Mostly related to `log.go` and log entries
	logInitialSize = 512 // Initial log capacity
	entrySizeBytes = 250 // Predefined (expected) size of a log entry

	//
	// Client-facing API
	// HTTP codes
	successStatusCode = 202 // HTTP status code for an accepted TX
	errorStatusCode   = 400 // HTTP status code for an erroneous TX

	tearDownSlack = 2 // Period (seconds) to wait for graceful HTTP exit
)

type nodeConf struct {
	cfgPath          string
	Position         int
	Listen           string
	Successors       []string
	Broadcast        bool
	IsSequencer      bool //We don't need this hard-code variable in the conf because the system can set the sequencer by configuration number
	FaultThreshold   int
	SystemSize       int
	StartReconfig    bool //hard-code variable for firing the reconfiguration
	ReConfigNetworks []string //These are the node addresses like the successors above

	// NumCarriers     int
	// RenewalPeriod   int
	// BodyMaxSizeKB   int
	// MaxOutstanding  int
}

func getNodeConf() *nodeConf {
	var c nodeConf

	flag.StringVar(&c.Listen, "listen", defaultListen,
		"`addr:port` of service")
	flag.StringVar(&c.cfgPath, "cfg", "conf/node1.json",
		"`path` to the JSON configuration file")

	flag.Parse()
	err := getConfJSON(c.cfgPath, &c)
	if err != nil {
		glog.Fatalf("Error parsing the JSON config file (%s)", c.cfgPath)
		os.Exit(-1)
	}

	// The fault threshold is computed as simply the number of successors - 1
	// TODO? make this more clean
	c.FaultThreshold = len(c.Successors) - 1

	glog.Info("Using configuration parameters:")
	glog.Infof("  -- listening on: .... %s", c.Listen)
	glog.Infof("  -- cfg:  ............ %s", c.cfgPath)
	glog.Infof("  -- Position: ........ %v", c.Position)
	glog.Infof("  -- Successors: ...... %v", c.Successors)
	glog.Infof("  -- Broadcast: ....... %v", c.Broadcast)
	glog.Infof("  -- IsSequencer: ..... %v", c.IsSequencer)
	glog.Infof("  -- StartReconfig: ..... %v", c.StartReconfig)
	glog.Infof("  -- FaultThreshold: .. %v", c.FaultThreshold)
	glog.Infof("  -- SystemSize: ...... %v", c.SystemSize)
	glog.Infof("  -- Networks for Reconfiguration: ...... %v", c.ReConfigNetworks)

	return &c
}

func getConfJSON(cgfFile string, c *nodeConf) error {
	configData, err := ioutil.ReadFile(cgfFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(configData, &c)
	if err != nil {
		return err
	}

	// Set defaults in case they're missing
	// ..
	// ..

	return nil
}
