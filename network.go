package main

import (
	"net"
	"io"
	"time"
	"sync"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"github.com/golang/glog"
)

// This module holds all the logic for communicating with successors and
// predecessors.
// High-level interaction:
//
// -----------------------------
// predecessor node(s)
// -----|--------| -------------
//      |*Msg    |*Msg     ^
//      V        V         |
//    BareMsg   ..         |
//      |        |         |Activate
//      |*Push   |         |
//      |        |         |
//      |________|         |
//      V                  |
//      pushed             |
// --------------|---------|---
//  local node
//     m *Msg ---|
//               |
//               V
//           outstanding
//               |
//       ________|___
//      |            |
//      |            |
//      V            V
//  outbound    outbound ....
// -----|-----------|----------
//      |..         |..
//      |..         |..
//      |..         |..
//      |*Msg       |*Msg
// -----V-----------V----------
// successor node(s)
// -----------------------------

// The Network type implements the NetworkLayerServer interface
type Network struct {
	log  *LocalLog
	node *R3Node
	conf *nodeConf

	// GRPC members; we'll need to keep track of connection streams
	// This is for client-side (i.e., conn to our successor)
	sstream [][]NetworkLayer_BareMsgClient

	sstreamLock sync.Mutex

	// GRPC member for serve-side (i.e., connection to our predecessor)
	server *grpc.Server

	// Keep track of whether our predecessor is a sequencer
	// Needed in case we replace this node
	predSeq bool

	//
	// Inbound and outbound channels
	//

	// One outbound channel for each stream to each successor
	outbound [][]chan *Msg

	// This channel holds any pushed messages from predecessor(s)
	pushed chan *Push

	// This is a multiplex channel; we'll split messages from here into one of
	// the outbound channels for each active successor.
	outstanding chan *Msg

	// Keep track of which stream we used last time, one per successor
	lastOutbound []int

	// Emulate a set with this map; maintains all activated successor paths
	ssactive map[int]struct{}

	// Set to true if we should send Activate message on fallback paths.
	fallback bool
}

func NewNetwork(conf *nodeConf, ll *LocalLog) *Network {
	addr := conf.Listen
	grpcServer := grpc.NewServer()

	l, err := net.Listen("tcp", addr)
	if err != nil {
		glog.Fatalf("Failed to listen on addr %v: %v", addr, err)
	}

	nSuccessors := len(conf.Successors)
	nReConfigNet := len(conf.ReConfigNetworks)
	nNet := nSuccessors + nReConfigNet

	nt := &Network{
		log:          ll,
		node:         nil,
		conf:         conf,
		server:       grpcServer,
		predSeq:      false,
		sstream:      make([][]NetworkLayer_BareMsgClient, nNet),
		outbound:     make([][]chan *Msg, nNet),
		pushed:       make(chan *Push, netChansCap),
		outstanding:  make(chan *Msg, netChansCap),
		lastOutbound: make([]int, nNet),
		ssactive:     make(map[int]struct{}),
		fallback:     false,
	}
	RegisterNetworkLayerServer(grpcServer, nt)

	// By default, our immediate successor is always active; dummy empty struct
	nt.ssactive[0] = struct{}{}

	go nt.server.Serve(l)
	glog.Infof("Network service started at %v", addr)

	return nt
}

func (nt *Network) Start() {

	nt.sstreamLock.Lock()
	defer nt.sstreamLock.Unlock()

	for spos, succ := range nt.node.Successors {
		nt.sstream[spos] = make([]NetworkLayer_BareMsgClient,
			pushPipelineWidth)
		nt.outbound[spos] = make([]chan *Msg, pushPipelineWidth)
		nt.lastOutbound[spos] = 0
		for conid := 0; conid < pushPipelineWidth; conid++ {
			// Multiple parallel connections to our successor
			nt.connectToSuccessor(&succ, spos, conid)
			glog.Infof("Successor pos %d. [%d] %s", spos, succ.Position,
				succ.Address)
			// Set the channel
			nt.outbound[spos][conid] = make(chan *Msg, netChansCap)
			go nt.processOutbound(spos, conid)
			go nt.processInbound(spos)
		}
		go nt.processPushedMessages(spos)
	}

	go nt.processOutstanding()
}

func (nt *Network) StartReconfigNet() {

	nt.sstreamLock.Lock()
	defer nt.sstreamLock.Unlock()

	for spos, net := range nt.node.ReConfigNetworks {
		index := spos + len(nt.node.Successors)
		nt.sstream[index] = make([]NetworkLayer_BareMsgClient,
			pushPipelineWidth)
		nt.outbound[index] = make([]chan *Msg, pushPipelineWidth)
		nt.lastOutbound[index] = 0
		for conid := 0; conid < pushPipelineWidth; conid++ {
			// Multiple parallel connections to our successor
			nt.connectToSuccessor(&net, index, conid)
			glog.Infof("Network for Reconfiguration pos %d. [%d] %s", index, net.Position,
				net.Address)
		}
	}

}

func (nt *Network) SetNode(n *R3Node) {
	nt.node = n
	if n.IsSequencer() == true {
		//nt.fallback = true
	}
}

func (nt *Network) activateSuccessor(sid int) {
	if _, found := nt.ssactive[sid]; found == false {
		glog.Infof("Activated the path to successor %d", sid)
		nt.ssactive[sid] = struct{}{}
	}
}

//
//
// Client side, i.e., predecessor on the ring
// Connect to successors, if any
func (nt *Network) connectToSuccessor(successor *SuccessorRef, spos int,
	cnid int) {
	if successor == nil {
		glog.Warning("Node has no succesor(s)!")
		return
	}

	glog.Infof("Connecting to our successor %v (connection #%d)",
		successor.Address, cnid)

	conn, err := grpc.Dial(successor.Address, grpc.WithInsecure(),
		grpc.WithBlock(), grpc.WithTimeout(10*time.Second))
	if err != nil {
		glog.Warningf("Failed to dial: %v", err)
	} else {
		nt.handshakeWithSuccessor(conn, successor.Address, spos, cnid)
	}
}

func (nt *Network) handshakeWithSuccessor(conn *grpc.ClientConn, sAddr string,
	spos int, conid int) {
	client := NewNetworkLayerClient(conn)

	glog.Infof("Connected. Handshake with succ %v. spos = %d, conid = %d",
		sAddr, spos, conid)

	stream, err := client.BareMsg(context.Background())
	if err != nil {
		glog.Fatalf("Error during handshake: %v", err)
	}

	// Save the stream for this successor for later use
	// TODO: should synchronize the access to nt.sstream
	nt.sstream[spos][conid] = stream
	nt.sendHello(spos, conid, int32(nt.conf.Position))
}

//
//
// Server side, i.e., implements the role of successor on the ring
// Handles the delivery of messages from predecessor(s)
//
func (nt *Network) BareMsg(stream NetworkLayer_BareMsgServer) error {
	var sender int32 = 0;
	addr, err := extractRemoteAddress(stream)
	if err != nil {
		glog.Warningf("New (unknown) stream registered: (%v, %v)", addr, err)
	} else {
		glog.Infof("New stream registered with predecessor: %v", addr)
	}

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			glog.Warning("Reached the end of stream")
		}
		if err != nil {
			glog.Warningf("Error while reading stream: %v", err)
			// Our predecessor crashed
			if nt.predSeq == true {
				glog.Warningf("Our predecessor was the sequencer. We'll take over that role.")
				// nt.node.BecomeSequencer(nt.log.MaxSN)
				panic("Not implemented!")
			}
			return err
		}

		// Check what kind of message this is
		if h := msg.GetHello(); h != nil {
			sender = h.GetPos()
			glog.Infof("[net] Sender announced: %v <-> %d", addr, sender)

		} else if s := msg.GetStatus(); s != nil {
			glog.Infof(" [net %d] status %v", sender, s.GetVector())
			if nt.fallback == true {
				glog.Infof("[net] Activating path from predecessor %d", sender)
				err := stream.Send(getActivateMsg(nt.conf.Position, false,
					NewNonce()))
				if err != nil {
					glog.Errorf("[net] Error activating %v", err)
				}
			}

		} else if p := msg.GetPush(); p != nil {
			glog.Warningf("[net %d] <- #%12d size=~%v |data|=%v, |seq|=%v",
				sender, msg.GetNonce(), MsgLength(msg), len(p.GetData()),
				len(p.GetSequence()))
			//if nt.node.ReconfigAlreadyStarted(){
			//	p.GetSequence()=nil
			//	glog.Infof("reconfig is already started, so sequence message is ignored")
			//}
			// TODO: Unclear if this pushed channel adds any value anymore.
			// Remove.
			nt.pushed <- p
		} else if r := msg.GetReconfig(); r != nil {
			if r.GetCn() >= nt.node.cn { //Ignore reconfiguration message which configuration number is smaller than current configuration number
				sender = r.GetNode()
				glog.Infof("[reconfig]:from node %d Old Configuration Number: %d the len of pendingSMSet: %d", sender, r.GetCn(), len(r.PendingSMSet.PendingSM))
				if len(r.PendingSMSet.PendingSM) != 0 {
					for k, v := range r.PendingSMSet.PendingSM {
						glog.Infof("Entry id: %s, %v", k, v)
					}
				}
				_, received := nt.node.reconfigFlags[sender]
				if !received && r.GetCn() >= nt.node.cn && !nt.node.NewConfigAlreadyStarted() {
					nt.node.reconfigFlags[sender] = struct{}{} //Mark the replicas which start reconfiguration
					if nt.node.predecessorIsSequencer() { //If replicas is new sequencer, gather the pending set from 4f+1 replicas
						for k, v := range r.GetPendingSMSet().PendingSM {
							_, found := nt.node.Log.reprocessSet[v.GetSn()]
							if !found {

								nt.node.Log.reprocessSet[v.GetSn()] = make(map[string]int32)
							}
							nt.node.Log.reprocessSet[v.GetSn()][k]++
						}
						nt.node.Log.gatherSet[sender] = r.PendingSMSet.PendingSM
						for k, v := range nt.node.Log.gatherSet {
							for _, v2 := range v {
								glog.Infof("[rset]:Node: %d, %v", k, v2)
							}
						}
					}
				}
				glog.Infof("[reconfig]: obsevered %d nodes start reconfiguration.", len(nt.node.reconfigFlags))
				if len(nt.node.reconfigFlags) == nt.node.reconfigThreshold && !nt.node.ReconfigAlreadyStarted() { //fire the reconfiguration
					glog.Infof("[reconfig]: start reconfiguration!")
					if nt.node.predecessorIsSequencer() {
						asseSet := nt.node.generateSMFromPendingSet().PendingSM
						for k, v := range asseSet {
							_, found := nt.node.Log.reprocessSet[v.GetSn()]
							if !found {
								nt.node.Log.reprocessSet[v.GetSn()] = make(map[string]int32)
							}
							nt.node.Log.reprocessSet[v.GetSn()][k]++
						}
						nt.node.Log.gatherSet[int32(nt.node.nodeID)] = asseSet
						for _, v := range nt.node.Log.gatherSet {
							for k2, v2 := range v {
								glog.Infof("[rset]:Entry id: %s, id: %s", k2, v2.Id)
							}
						}
					}
					go nt.node.ReconfigSend()
				}
				if nt.node.predecessorIsSequencer() &&
					(len(nt.node.reconfigFlags) == nt.node.newconfigThreshold ||  // 4f+1
						(len(nt.node.reconfigFlags)+1 == nt.node.newconfigThreshold && nt.node.ReconfigAlreadyStarted())) && // 4f and itself
					!nt.node.NewConfigAlreadyStarted() {
					glog.Infof("[new config]: New configuration starts!")
					nt.node.StopReconfig()
					nt.node.NewConfigSend()
					timer := time.NewTimer(time.Second * 20)
					<-timer.C
					nt.node.StopNewConfig()
					nt.reprocessSM() //redo the common-case agreement protocol
				}
			}
		} else if n := msg.GetNewconfig(); n != nil {
			if n.GetCn() > nt.node.cn {
				sender = n.GetCn() % int32(nt.conf.SystemSize)
				glog.Infof("[new config]: from node %d, New Configuration Number: %d", sender, n.GetCn())
				nt.node.StopReconfig()
				for k, v := range n.GetRset() {
					for k2, v2 := range v.PendingSM {
						glog.Infof("[new config]: Node: %d, Sequence Number: %d, Entry id: %s", k, v2.Sn, k2)
					}
				}
				nt.node.cn = n.GetCn()
				nt.node.StopNewConfig()
			}
		} else {
			glog.Warningf("Unknown message: %v!", msg)
		}
	}
}

// Outstanding messages are waiting to be batched and assigned to be pushed
// via one of our connections to each active successor
func (nt *Network) processOutstanding() {
	for {
		// Drain the outstanding channel, if any
		m := <-nt.outstanding
		if p := m.GetPush(); p != nil {
			glog.Warningf("[net] -> #%12d size=~%v |d|=%v, |s|=%v",
				m.GetNonce(), MsgLength(m), len(p.GetData()),
				len(p.GetSequence()))
		} else {
			glog.Errorf("Unknown outstanding message! (%v)", m)
			panic("Unknown outstanding message")
		}

		for spos := range nt.ssactive {
			conn := nt.pickConnection(spos)
			nt.outbound[spos][conn] <- m
		}
	}
}

func (nt *Network) sendHello(spos int, conn int, mypos int32) {
	m := &Msg{
		Type: &Msg_Hello{&Hello{Pos: mypos}},
	}

	nt.sendViaConnection(m, spos, conn, false)
}

func (nt *Network) SendStatusVector(sv []uint64) {
	m := &Msg{
		Type: &Msg_Status{&Status{Vector: sv}},
	}
	// We send SVs on all secondary forwarding paths
	for i := 1; i < len(nt.node.Successors); i++ {
		nt.sendViaConnection(m, i, 0, false)
	}
}

func (nt *Network) SendReConfig(conn int) {
	m := nt.node.assembleReconfigMsg()
	nt.StartReconfigNet()
	nt.node.reconfigSendTicker = time.NewTicker(nt.node.reconfigSendDuration)
	for t := range nt.node.reconfigSendTicker.C {
		glog.Info("[reconfig]: reconfig ", t)
		for i := 0; i < len(nt.sstream); i++ {
			nt.sendViaConnection(m, i, conn, false)
		}
	}
}

func (nt *Network) SendNewConfig(conn int) {
	m := nt.node.assembleNewConfigMsg()
	glog.Infof("Rset length: %d", len(m.GetNewconfig().GetRset()))
	nt.StartReconfigNet()
	for i := 0; i < len(nt.sstream); i++ {
		nt.sendViaConnection(m, i, conn, false)
	}

}

func (nt *Network) sendViaConnection(m *Msg, spos int, conn int, retry bool) {
	if nt.sstream[conn] != nil {
		if err := nt.sstream[spos][conn].Send(m); err != nil {
			glog.Warningf("Error sending to succ (%s)! Bypassing this node.",
				err)

			if retry == true {
				// We shouldn't lose this message, re-enqueue to retry later
				nt.addOutstandingMsg(m)
			} else {
				glog.Info("Not an important msg; will not retry.")
			}

			// Re-connect to our successor2
			nt.sstream[conn] = nil
			panic("Attempting to switch to a diff successor. (not implemented)")
			// nt.connectToSuccessor(nt.successor2Ref, conn)
		}
	}
	// else {
	//     glog.Infof("Not connected to successor yet!")
	// }
}

// Returns the index for a specific connection to be used when sending
// something to our successor
func (nt *Network) pickConnection(spos int) int {
	// Round-robin selection
	nt.lastOutbound[spos] = (nt.lastOutbound[spos] + 1) % pushPipelineWidth
	return nt.lastOutbound[spos]
}

// Handles messages which arrived from the predecessor.
// All messages processed here are of type Push.
func (nt *Network) processPushedMessages(spos int) {
	for {
		select {
		case p := <-nt.pushed:
			nt.node.HandlePush(p)
		}
	}
}

// The routine which drains the outbound channel to a given successor
// for a given connection cnid.
// TODO: consider removing the outbound mechanism.
func (nt *Network) processOutbound(spos int, cnid int) {
	for {
		select {
		// It is time to push something to our successor
		case m := <-nt.outbound[spos][cnid]:
			nt.sendViaConnection(m, spos, cnid, false)
		}
	}
}

// The routine which consumes incoming messages from a given successor
// for a given connection cnid
func (nt *Network) processInbound(spos int) {
	inbound := make(chan *Msg, 10)
	nt.sstreamLock.Lock()

	// Merge all messages coming from any stream to this successor into a
	// single channel
	for idx, tstream := range nt.sstream[spos] {
		go func() {
			for {
				if tstream != nil {
					// Wait for incoming messages from our successor
					msg, err := tstream.Recv()
					if (err == io.EOF) || (msg == nil) {
						glog.Warningf("[net] Ending stream with succ %d #%d",
							spos, idx)
						return
					}
					// Got a correct message, most likely "Activate"
					inbound <- msg

				} else {
					glog.Errorf("%d #%d NIL stream reference", spos, idx)
					panic("Cannot recover.")
				}
			}
		}()
	}
	nt.sstreamLock.Unlock()

	for {
		inb := <-inbound // Waits for messages
		glog.Infof("[net] <- from succ %d: %v", spos, inb)
		if a := inb.GetActivate(); a != nil {
			nt.activateSuccessor(spos)
		}
	}
}

func (nt *Network) addOutstandingMsg(m *Msg) {
	nt.outstanding <- m
}

func (n *Network) stopNetwork() {
	n.server.GracefulStop()
}

func (nt *Network) reprocessSM() {
	var sbuf = make([]*Sequence, 0, 10)
	var dbuf = make([]*Data, 0, 10)
	for k, v := range nt.node.Log.reprocessSet {
		if k != 0 {
			vouched := make(map[string]struct{})
			for k2, v2 := range v {
				if v2 >= int32(2*nt.node.Conf.FaultThreshold+1) {
					vouched[k2] = struct{}{}
				}
			}

			// If len(vouched) == 1, which means there is only one pair vouched, so we just pick this sequence message
			// If there are more than one pairs vouched,because the map is unordered, so just pick one sequence message from the map
			if len(vouched) >= 1 {
				counter := 0
				for k3 := range vouched {
					if counter < 1 {
						sbuf = append(sbuf, getSequenceMsgForData(k3, nt.node.generateID(), k, nt.node.Log.pendingSet[k3].Body, nt.node.cn))
						counter++
					}
				}
			} else { //No vouched pair
				counter := 0
				for k3 := range v {
					if counter < 1 {
						sbuf = append(sbuf, getSequenceMsgForData(k3, nt.node.generateID(), k, nt.node.Log.pendingSet[k3].Body, nt.node.cn))
						counter++
					}
				}
			}
		}
	}
	m := assemblePushMsg(dbuf, sbuf, NewNonce())
	nt.node.broadcastMsg(m)
	nt.node.Log.reprocessSet = make(map[uint64]map[string]int32)
}
