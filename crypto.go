package main

import (
	"crypto/md5"
	"bytes"
	"math/rand"
)

func NewNonce() uint32 {
	return rand.Uint32()
}

func computeDigest(entries [][]byte) []byte {
	var conc [][]byte
	for _, ex := range entries {
		conc = append(conc, ex[0:5]) // SN and ID (unique bytes) are in the prefix
	}

	// Flatten the hashes by joining them in a single []byte
	return hash(bytes.Join(conc, nil))
}

func hash(d []byte) []byte {
	s := md5.Sum(d)
	return s[0:5] // The 5-byte prefix suffices
}
