package main

// The methods in this module handle all the client-side related functionality.

import (
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"github.com/golang/glog"
	"gopkg.in/tylerb/graceful.v1"
)

type ClientService struct {
	node *R3Node
}

func NewClientService() *ClientService {
	cs := &ClientService{} // Nothing to initialize
	return cs
}

func (cs *ClientService) SetNode(n *R3Node) {
	cs.node = n
}

func (cs *ClientService) Run() {
	glog.Infof("Client service starting on %s", defaultClientHTTPListen)

	// Start the ClientService HTTP API
	mux := http.NewServeMux()
	mux.Handle("/tx/", cs)

	var backoff int32 = 10
	for {
		// Ensure graceful tear-down
		// Upon success, the following call blocks
		err := graceful.RunWithErr(defaultClientHTTPListen,
			tearDownSlack*time.Second, mux)
		if err != nil {
			glog.Warningf("Client service could not start. Error: %s", err)
			glog.Warningf("Retrying in %d seconds..", backoff)
			glog.Flush()

			time.Sleep(time.Duration(rand.Int31n(backoff)) * time.Second)
			backoff += backoff
		}
	}
}

// the web server handler of the local clientService
func (cs *ClientService) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	// client request
	case "PUT":
		glog.Infof("-. PUT")
		t0 := time.Now()
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			glog.Warningf("Error reading the TX body!(%s)", err)
			return
		}

		// Broadcast the request
		notifCh := cs.node.BroadcastBatch(body)
		if notifCh != nil {
			t1 := time.Now()
			glog.Infof("[] Broadcast Request took %v", t1.Sub(t0))
			glog.Infof("[] Waiting for response..")
			<-notifCh // Wait until the broadcast operation finishes
			t2 := time.Now()
			glog.Infof("[] Broadcast Reponse took %v", t2.Sub(t1))
			cs.respondHTTPSuccess(w)
		} else {
			cs.respondHTTPError(w)
		}
	default:
		glog.Warningf("Unknown method (%s) not handled", r.Method)
	}
}

func (cs *ClientService) respondHTTPSuccess(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(successStatusCode)
	io.WriteString(w, "queued\n")
}

func (cs *ClientService) respondHTTPError(w http.ResponseWriter) {
	w.WriteHeader(errorStatusCode)
	io.WriteString(w, "malformed request\n")
}
