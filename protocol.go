package main

import (
	"fmt"
	"strings"
	"strconv"
	"sync/atomic"

	"github.com/golang/glog"
)

// Handles the interpretation of protocol messages.

func (node *R3Node) HandlePush(p *Push) {
	var sbuf []*Sequence = make([]*Sequence, 0, 10)
	var dbuf []*Data = make([]*Data, 0, 10)

	// Iterate over all Data messages
	for _, dm := range p.GetData() {
		if node.isLocallyGenerated(dm.GetId()) {
			// This entry did one full round. We should never get it: it should
			// have stopped at our predecessor.
			glog.Warningf("[%s] was locally generated", dm.GetId())

		} else {
			// Handle the Data message; might generate a Sequence msg
			sm, seen := node.handleData(dm)
			if sm != nil {
				sbuf = append(sbuf, sm)
			}

			// Now decide if we need to continue pushing this message.
			// This should only happen if it wasn't our successor who generated
			// this message.
			if (seen == false) &&
				(node.isSuccessorGenerated(dm.GetId()) == false) {
				// Continue propagation of the Data message until it reaches the
				// predecessor of the node which generated it.
				dbuf = append(dbuf, dm)
				glog.Infof("Continuing propagation of %s", dm.GetId())
			} else {
				glog.Infof("Can stop propagating %s", dm.GetId())
			}
		}
	}

	// Iterate over all Sequence messages
	for _, sm := range p.GetSequence() {
		// glog.Infof(".Sequence %v", sm.GetId())
		// Handle the Sequence message; might generate our own Sequence msg
		if !node.ReconfigAlreadyStarted() {
			newsm, seen := node.handleSequence(sm)
			if newsm != nil {
				sbuf = append(sbuf, newsm)
			}
			if (seen == false) && (node.isSuccessorGenerated(sm.GetId()) == false) {
				// Continue propagation of the Data message until it reaches the
				// predecessor of the node which generated it.
				sbuf = append(sbuf, sm)
			}
		}else {
			glog.Infof("Ignore sequence message")
		}
	}

	// If there's anything left to send, send it
	if len(dbuf) > 0 || len(sbuf) > 0 {

		//fire the reconfiguration of the successor by stoping propagating sequence message.
		if node.shouldFireReconfig(){
			sbuf = make([]*Sequence, 0, 10)
			node.fireReconfig = false
		}
		m := assemblePushMsg(dbuf, sbuf, NewNonce())
		// glog.Info("Resulting push: %v", m)

		node.broadcastMsg(m)
	}
}

// Handles the delivery of a Data message.
// May generate a Sequence message to establish the SN for the given Data.
// Return false if this Data message was already processed, true otherwise.
// TODO: maybe do this more efficiently, using a single call to the log.
func (node *R3Node) handleData(dm *Data) (*Sequence, bool) {
	glog.Infof("data: %s", dm)
	node.Log.AddPending(dm)
	marked := node.markStateVector(dm.GetId())

	// If this entry has no SN and we're the sequencer, we'll assign one.
	// Make sure we haven't yet voted for this id.
	if (node.IsSequencer() == true) && (marked == true) {
		sm := getSequenceMsgForData(dm.GetId(), node.generateID(),
			node.generateSN(), dm.GetEntry(), node.cn)
		node.Log.UpdatePending(sm)
		node.markStateVector(sm.GetId())

		return sm, !marked
	} else {
		return nil, !marked
	}
}

func (node *R3Node) handleSequence(sm *Sequence) (*Sequence, bool) {
	if err := node.Log.UpdatePending(sm); err != nil {
		glog.Warningf("Error handling Sequence msg: %v", err)
		// Error was thrown because this Sequence message was out of order
		node.Log.AddPending(getDataMsg(sm.GetDid(), nil))

		// Try again
		return node.handleSequence(sm)
	}
	marked := node.markStateVector(sm.GetId())

	// Generate our sequence message if we haven't already and if this entry is
	// not yet stable.
	if (node.Log.HasVote(sm.GetDid(), node.nodeID) == false) &&
		(node.Log.isSNStable(sm.GetSn()) == false) {
		nsm := getSequenceMsg(sm.GetDid(), node.generateID(), sm.GetSn(),
			sm.GetHash(), node.cn)
		node.Log.UpdatePending(nsm)
		node.markStateVector(nsm.GetId())
		return nsm, !marked
	}

	return nil, !marked
}

// Update the state vector of the local node.
// Returns false if the state vector already had the value.
// Returns true if the state vector was successfully marked for this timestamp.
func (node *R3Node) markStateVector(id string) bool {
	n, nTs := parseMessageId(id)

	node.svLock.Lock()
	defer node.svLock.Unlock()

	if nTs > node.sv[n] {
		// Mark any holes
		for i := node.sv[n] + 1; i < nTs; i++ {
			node.svh[n][i] = struct{}{}
		}

		// Advance the SV upper limit
		node.sv[n] = nTs
		return true

	} else {
		// The SV upper rests unchanged
		// We might need fill a hole
		if _, found := node.svh[n][nTs]; found == true {
			delete(node.svh[n], nTs)

			return true
		} else {
			return false
		}
	}
}

//
//
// Various helpers

// If this node is the sequencer, it returns a monotonically-increasing number
// otherwise, it returns 0
func (node *R3Node) generateSN() uint64 {
	if node.IsSequencer() == true {
		nsn := atomic.AddUint64(&node.lastSN, 1)
		if nsn%1000 == 0 {
			glog.Warningf("SN currently at: %d", nsn)
		}
		return nsn
	} else {
		return 0
	}
}

// Splits a message identifier in the pair (node id, timestamp)
func parseMessageId(tid string) (int, uint64) {
	tokens := strings.SplitN(tid[1:], "/", 2)

	// Obtain the node's identifier.
	nid, err := strconv.Atoi(tokens[0])
	if err != nil {
		glog.Errorf("Error parsing msg ID: %s; node id=%d (%s)", tid, nid, err)
	}

	// Obtain the timestamp from the message.
	nts, err := strconv.ParseUint(tokens[1], 10, 64)
	if err != nil {
		glog.Errorf("Error parsing msg ID: %s; node ts=%v (%s)", tid, nts, err)
	}

	return nid, nts
}

// Generates a unique identifier to be used in messages sent by this node
func (node *R3Node) generateID() string {
	nts := atomic.AddUint64(&node.lastTS, 1)
	if nts%1000 == 0 {
		glog.Infof("Generated new ID from %d", nts)
	}
	return fmt.Sprintf("/%v/%v", node.nodeID, nts);
}

// Given an entry ID; returns true if the entry was generated by this node.
// Returns false otherwise.
func (node *R3Node) isLocallyGenerated(id string) bool {
	return strings.Contains(id, fmt.Sprintf("/%v/", node.nodeID))
}

// Inspects the entry ID; returns true if the entry was generated by the
// successor of this node
func (node *R3Node) isSuccessorGenerated(id string) bool {
	return strings.Contains(id, fmt.Sprintf("/%v/", node.MainSuccessor))
}
