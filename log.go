package main

import (
	"errors"
	"sync"

	"github.com/golang/glog"
	"time"
)

const (
	// The default sequence number; if an entry has this sequene number, then
	// there was no SN proposed for that entry.
	NIL_SN = 0
)

type LogEntry struct {
	Body   []byte           // The body, typically an opaque payload
	Header LogEntryHeader   // Header (see below)
	Votes  map[int]struct{} // Counter on the number of votes for this entry
	Hash 	[]byte  //Hash of the entry
}

type LogEntryHeader struct {
	ID string // The id of the entry, specific to the source node
	SN uint64 // The sequence number of the entry
}

type LocalLog struct {
	// Log-specific members
	stableSet  map[uint64]*LogEntry
	pendingSet map[string]*LogEntry

	gatherSet     map[int32]map[string]*Sequence //This gatherSet is the new sequencer gathered sequence messages from 4f+1 replicas' pendingSet key1:key2:value : nodeId:id:*sequence
	reprocessSet map[uint64]map[string]int32 //This is gatherSet after some processing, key1:key2:value : sn:did:number of sequence message vouched {sn,id}
	stableLock    sync.RWMutex
	pendingLock   sync.RWMutex
	reprocessLock sync.RWMutex

	// Keep track of the maximum observed SN value
	MaxSN uint64

	// Minimal number of votes needed for an entry to be promoted to stable
	stableVotes int

	// References to other objects on this node
	node *R3Node

	// Transitions of LogEntry from pending to stable state will sometimes be
	// announced to clients via a channel
	notifRqs     map[string]chan<- bool
	notifRqsLock sync.RWMutex
}

//
//
// Log functionality

func NewLog(minVotes int) *LocalLog {
	glog.Infof("Min. votes for stable entries: %v", minVotes)

	return &LocalLog{
		stableSet:  make(map[uint64]*LogEntry),
		pendingSet: make(map[string]*LogEntry),

		gatherSet:   make(map[int32]map[string]*Sequence),
		reprocessSet:make(map[uint64]map[string]int32),
		notifRqs:    make(map[string]chan<- bool),
		MaxSN:       0,
		stableVotes: minVotes,
	}
}

func (log *LocalLog) SetNode(n *R3Node) {
	log.node = n
}

func (log *LocalLog) addPendingEntry(le *LogEntry) {
	if len(log.pendingSet)%10000 == 0 {
		glog.Warningf("Pending length = %d; adding (%s:%d).",
			len(log.pendingSet), le.Header.ID, le.Header.SN)
	}

	log.pendingLock.Lock()
	defer log.pendingLock.Unlock()
	if _, found := log.pendingSet[le.Header.ID]; found == false {
		log.pendingSet[le.Header.ID] = le // pendingSet is indexed by the ID
	} else {
		// TODO
		glog.Warningf("Data message already existed! Should update body?")
	}
}

func (log *LocalLog) AddPending(d *Data) {
	le := &LogEntry{
		Header: LogEntryHeader{
			ID: d.GetId(),
			SN: NIL_SN,
		},
		Body:  d.GetEntry(),
		Votes: make(map[int]struct{}),
		Hash: hash(d.GetEntry()),
	}

	log.addPendingEntry(le)

	log.node.reconfigStartTimer = time.NewTimer(log.node.reconfigStartDuration) //start timer of to expect sequence messages for the certain data.
	glog.Infof("Timer is started, if the timer expried, the reconfiguration will start!")
	go func() {
		<- log.node.reconfigStartTimer.C
		glog.Infof("Timer is expired, the reconfiguration will start.")
		go log.node.ReconfigSend()
	}()

}

// Updates a pending entry to include the given Sequence message.
// May promote the entry to the stable log if sufficient votes were gathered.
func (log *LocalLog) UpdatePending(s *Sequence) error {
	// The node which sent this message is encoded in the message id
	nodeId, _ := parseMessageId(s.GetId())
	if nodeId==int(log.node.cn){
		log.node.reconfigStartTimer.Stop()
		glog.Infof("Timer stopped")
	}
	log.pendingLock.Lock()

	entry, found := log.pendingSet[s.GetDid()]

	if found == true {
		// Decide if we have to update the sequence number
		if entry.Header.SN == NIL_SN {
			entry.Header.SN = s.GetSn()

		} else if entry.Header.SN != s.GetSn() {
			glog.Errorf("Conflicting sequence numbers for %s! (%v != %v)",
				s.GetDid(), entry.Header.SN, s.GetSn())
			// Unrecoverable error (?)
			panic("Conflicting sequence numbers!")
		}

		// Now count the vote
		entry.Votes[nodeId] = struct{}{}
		glog.Infof("Voting for sequence %d to %s (sequence id=%s)", s.GetSn(), s.GetDid(), s.GetId())
		log.printPendingSet(log.pendingSet)

		// Decide if we promote the entry
		if len(entry.Votes) >= log.stableVotes {
			glog.Infof("Promoting (%s:%d) to stable", s.GetDid(), s.GetSn())
			log.pendingLock.Unlock()
			log.RemovePendingEntry(entry.Header.ID)
			log.AddStableEntry(entry)
		} else {
			log.pendingLock.Unlock()
		}
	} else {
		// Entry was not found in pending. Maybe it's already stable
		log.pendingLock.Unlock()
		log.stableLock.RLock()
		defer log.stableLock.RUnlock()

		if _, sfound := log.stableSet[s.GetSn()]; sfound != true {
			glog.Warningf("Entry id %s not found in pending nor stable!",
				s.GetDid())
			return errors.New("Sequence message is out of order!")
		}
	}
	return nil
}

// Returns true if the node with `nodeId` has voted for entry with `id`
func (log *LocalLog) HasVote(id string, nodeId int) bool {
	log.pendingLock.RLock()
	defer log.pendingLock.RUnlock()

	entry, found := log.pendingSet[id]
	if found == true {
		_, okf := entry.Votes[nodeId]
		return okf
	}
	return false
}

func (log *LocalLog) HasSN(id string) bool {
	log.pendingLock.RLock()
	defer log.pendingLock.RUnlock()

	_, found := log.pendingSet[id]
	return found
}

func (log *LocalLog) isSNStable(sn uint64) bool {
	log.stableLock.RLock()
	defer log.stableLock.RUnlock()

	_, found := log.stableSet[sn]
	return found
}

func (log *LocalLog) RemovePendingEntry(id string) *LogEntry {
	if len(log.pendingSet)%10000 == 0 {
		glog.Warningf("Pending length = %d; removing (%s).",
			len(log.pendingSet),
			id)
	}
	log.pendingLock.Lock()
	defer log.pendingLock.Unlock()

	entry, found := log.pendingSet[id]
	if found == true {
		delete(log.pendingSet, id)
		return entry
	} else {
		return nil
	}
}

func (log *LocalLog) AddStableEntry(e *LogEntry) {
	if len(log.stableSet)%10000 == 0 {
		glog.Warningf("Stable length = %d; adding (%s:%d)",
			len(log.stableSet),
			e.Header.ID,
			e.Header.SN)
	}

	// Announce any clients
	log.triggerNotification(e.Header.ID)

	// Update the maximum observed SN
	if e.Header.SN > log.MaxSN {
		log.MaxSN = e.Header.SN
	}

	log.stableLock.Lock()
	defer log.stableLock.Unlock()

	log.stableSet[e.Header.SN] = e
}

func (log *LocalLog) RemoveStableEntry(e *LogEntry) {
	panic("Not implemented! Is there ever a case to remove a stable entry?")
}

//
//
// Transition notifications

func (log *LocalLog) NewNotificationRequest(id string) <-chan bool {
	notifCh := make(chan bool, 1)

	log.notifRqsLock.Lock()
	defer log.notifRqsLock.Unlock()

	log.notifRqs[id] = (chan<- bool)(notifCh)

	return (<-chan bool)(notifCh)
}

func (log *LocalLog) triggerNotification(id string) {
	// quick check whether we have any notification requests for this id
	log.notifRqsLock.RLock()
	if log.notifRqs[id] == nil {
		log.notifRqsLock.RUnlock()
		return
	} else {
		log.notifRqsLock.RUnlock()
	}

	log.notifRqsLock.Lock()
	defer log.notifRqsLock.Unlock()

	if ch := log.notifRqs[id]; ch != nil {
		ch <- true
		delete(log.notifRqs, id)
	}
}

//
//
// LogEntry and LogEntryHeader functionality

func (le *LogEntry) GetID() string {
	return le.Header.ID
}

func NewLogEntry(id string, sn uint64, body []byte) *LogEntry {
	le := new(LogEntry)
	le.Header.ID = id
	le.Header.SN = sn
	le.Body = body
	le.Hash = hash(body)

	return le
}

func LogEntryHeaderFromLogEntry(le *LogEntry) *LogEntryHeader {
	return &LogEntryHeader{
		ID: le.Header.ID,
		SN: le.Header.SN,
	}
}

func (log *LocalLog) printPendingSet(pendingSet map[string]*LogEntry) {
	for key, value := range pendingSet {
		glog.Infof("entry id: %v, sequence number: %d, entry header id: %v, votes number: %d", key, value.Header.SN, value.Header.ID, len(value.Votes))
	}
}
