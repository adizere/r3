SCRIPTNAME=$0


./evaluation/replicas/r3-control-cluster.sh deploy-conf
./evaluation/stressor-longrun.sh


cp -b conf/replicas.txt /tmp/replicas.txt
for (( i = 0; i < 5; i++ )); do
    tlc=$(wc -l conf/replicas.txt | awk '{print $1}')
    sed -i $((1 + RANDOM % $tlc))d conf/replicas.txt
done

bash "${SCRIPTNAME}"
