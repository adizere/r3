#!/bin/bash


function usage() {
    echo -e "\nUsage:\n\t`basename "$0"` COMMAND\n" >&2
    echo -e "COMMANDS:\n"
    echo -e "\tcreate l         Creates new instances based on config 'l'." >&2
    echo -e "\tcreate-simple l  Creates l new instances." >&2
    echo -e "\tcancel           Cancels all instances except the one tagged as 'coordinator'.\n" >&2
}



function create() {
    local config=$1

    if [[ ! -r ${config} ]]; then
        echo -e "Need a config file for the 'create' target!"
        usage;
        exit;
    fi

    cat $config | while read line; do
        rhostn=$(echo $line | awk '{print $1}')
        region=$(echo $line | awk '{print $2}')

        echo "creating instance" $rhostn "in region $region"
        echo -e "y" | bx sl vs create -D r3.lpd.epfl.ch -k 975685 \
            --wait 5 --image 1754269 -d $region -H $rhostn -c 2 -m 4096
    done
}


function create_simple() {
    local limit=$1

    if [[ -z ${limit} ]]; then
        echo -e "Need a limit for the 'create' target!"
        usage;
        exit;
    fi

    n=45
    suffix="rep"
    regions='sjc01'

    echo -e "Parameters: "
    echo -e "\t Limit :   ${limit}"
    echo -e "\t Start :   ${n}"
    echo -e "\t Suffix :  ${suffix}"
    echo -e "\t Regions : ${regions}"

    _dally;

    while [[ true ]]; do
        for r in ${regions} ; do

            if [[ $n -lt 10 ]]; then
                hostname="0$n-${suffix}"
            else
                hostname="$n-${suffix}"
            fi

            echo "creating instance " $hostname "in region $r"
            echo -e "y" | bx sl vs create -D r3.lpd.epfl.ch -k 975685 \
                --wait 5 --image 1754269 -d $r -H $hostname -c 2 -m 4096

            sleep 1;

            n=$(($n+1))

            if [[ $n -gt ${limit} ]]; then
                echo "done"
                exit;
            fi
        done
    done
}


function cancel() {
    list=$(slcli vs list)
    ids=$(echo "${list}" | grep -v coordinator | awk '{print $1}')
    for id in $(echo ${ids}); do
        host=$(echo "${list}" | grep ${id} | awk '{print $2}')
        dc=$(echo "${list}" | grep ${id} | awk '{print $5}')

        echo "Canceling device ${host} / ${id} from ${dc}"
        echo "${id}" | slcli vs cancel ${id}
    done
}


function list() {
    list=$(bx sl vs list)
    ids=$(echo "${list}" | grep -v coordinator | awk '{print $1}')
    for id in $(echo ${ids}); do
        host=$(echo "${list}" | grep ${id} | awk '{print $2}')
        dc=$(echo "${list}" | grep ${id} | awk '{print $5}')
        ip=$(echo "${list}" | grep ${id} | awk '{print $3}')

        echo "${host} : ${dc} : ${ip}"
    done
}

function _dally() {

    echo -e "\nPress any key to confirm (or CTRL^C to cancel) .."
    read ak
}



# Starts here..

if [[ -z $1 ]]; then
    usage;
    exit 1;
fi

case "$1" in
        create-simple)
            create_simple $2;
            exit 0;
        ;;
        create)
            create $2;
            exit 0;
        ;;
        cancel)
            cancel;
            exit 0;
        ;;
        list)
            list;
            exit 0;
        ;;
        *)
            usage;
            exit 1;
        ;;
esac
