#!/bin/bash

# Export the authentication parameters via ENV
ak=$(grep 'aws_access_key_id' ~/.aws/config | awk '{print $3}')
sk=$(grep 'aws_secret_access_key' ~/.aws/config | awk '{print $3}')

export AWS_ACCESS_KEY=${ak}
export AWS_SECRET_KEY=${sk}

REGIONS='eu-west-1 eu-west-2 eu-central-1 us-east-1 us-east-2'


DALLY_TIME=3
DEFAULT_AMI_NAME='r3-0.1'
INSTANCE_TYPE='t2.medium'
KEY_NAME="now-key"

function usage()
{
    echo -e "\nUsage:\n\t`basename "$0"` list|list-ips|stop|start|authorize|create <config-file> <ami-name>=${DEFAULT_AMI_NAME}" >&2

    echo -e "\n For a sample <config-file>, see conf/ec2-create-instances.config" >&2
}


function list() {
    local restrict_ips=$1
    if [[ $1 == "ips" ]]; then
        # We only need to list the IPs

        for region in ${REGIONS}; do

            raw=$(ec2-describe-instances --region ${region})

            instances=$(echo "${raw}" | grep INSTANCE | grep -v 'terminated' | awk '{print $2}')

            for i in $(echo ${instances}) ; do
                running=$(echo "${raw}" | grep "${i}.*running")
                if [[ ! -z "${running}" ]]; then
                    ip=$(echo "${raw}" | grep ${i} | grep -o -E "monitoring-disabled\s+\S+" | grep -o -E "[0-9\.]+")

                    # check whether there is any tag, e.g. 'client'
                    tag=$(echo "${raw}" | grep "TAG.*${i}" | awk '{print $5}')
                    if [[ ! -z ${tag} ]]; then
                        echo -e "${ip}\t${tag}"
                    else
                        echo -e "${ip}"
                    fi
                fi


            done
        done
        return
    fi

    # We list everything here
    for region in ${REGIONS}; do

        raw=$(ec2-describe-instances --region ${region})

        instances=$(echo "${raw}" | grep INSTANCE | grep -v 'terminated' | awk '{print $2}')
        echo -e "\n${region}"

        for i in $(echo ${instances}) ; do
            running=$(echo "${raw}" | grep "${i}.*running")
            if [[ ! -z "${running}" ]]; then
                ip=$(echo "${raw}" | grep ${i} | grep -o -E "monitoring-disabled\s+\S+" | grep -o -E "[0-9\.]+")
                # check whether there is any tag, e.g. 'client'
                tag=$(echo "${raw}" | grep "TAG.*${i}" | awk '{print $5}')
                if [[ ! -z ${tag} ]]; then
                    echo -e "\t-${i}\t${ip}\t${tag}"
                else
                    echo -e "\t-${i}\t${ip}"
                fi

            else
                echo -e "\t - ${i} \t stopped"
            fi

        done
        echo -e "\t---"
    done
}


function start_all() {
    # Which regions should we consider
    if [[ -z "$2" ]]; then
        regs=${REGIONS}
    else
        regs=$2
    fi

    echo -e "\t Regions: $regs\n"

    for region in ${regs}; do

        raw=$(ec2-describe-instances --region ${region})

        instances=$(echo "${raw}" | grep INSTANCE | grep -v 'terminated' | awk '{print $2}')
        echo -e "\n${region}"

        for i in $(echo ${instances}) ; do
            running=$(echo "${raw}" | grep "${i}.*running")

            if [[ -z "${running}" ]]; then
                # Now actually start them
                res=$(ec2-start-instances --region ${region} ${i})
                echo -e "\t ${i} started (${res})"
            else
                echo -e "\t - ${i} already running"
            fi

        done
        echo -e "\t---"
    done
}


function stop_all() {
    # Which regions should we consider
    if [[ -z "$2" ]]; then
        regs=${REGIONS}
    else
        regs=$2
    fi

    echo -e "\t Regions: $regs\n"

    for region in ${regs}; do

        raw=$(ec2-describe-instances --region ${region})

        instances=$(echo "${raw}" | grep INSTANCE | grep -v 'terminated' | awk '{print $2}')
        echo -e "\n${region}"

        for i in $(echo ${instances}) ; do
            running=$(echo "${raw}" | grep "${i}.*running")

            if [[ ! -z "${running}" ]]; then
                # Now actually stop them
                res=$(ec2-stop-instances --region ${region} ${i})
                echo -e "\t ${i} stopped (${res})"
            else
                echo -e "\t - ${i} already stopped"
            fi

        done
        echo -e "\t---"
    done
}


function create() {
    # Parameters for creating instances (number & regions)
    if [[ -s "$2" ]]; then
        config=$2
    else
        echo "Need a configuration for creating instances!"
        return
    fi

    local ami_name=$3
    # The AMI name to use
    if [[ -z "${ami_name}" ]]; then
        ami_name=${DEFAULT_AMI_NAME}
    fi

    echo -e "Using:"
    echo -e "\t configuration file: ... '$2'"
    echo -e "\t AMI name:  ............ '${ami_name}'"

    for i in $(grep -o -E "^[a-z\-]+\d\:\d+$" ${config}) ; do
        region=$(echo "${i}" | cut -d':' -f 1)
        count=$(echo "${i}" | cut -d':' -f 2)

        echo -e "\nLaunching ${count} instance(s) in ${region}"

        # Get the AMI ID
        ami=$(ec2-describe-images --region ${region} | grep "${ami_name}\s" | awk '{print $2}')

        if [[ -z "${ami}" ]]; then
            echo -e "Unable to find the AMI in ${region}. Stopping."
            return
        fi

        # Get the VPC ID
        subnet=$(ec2-describe-subnets --region ${region} | awk '{print $2; exit;}')
        if [[ -z "${subnet}" ]]; then
            echo -e "Unable to find the subnet in ${region}. Stopping."
            return
        fi

        echo -e "\t AMI ID: ${ami}"
        echo -e "\t Subnet ID: ${subnet}"
        echo -e "\t Launching using: 'ec2-run-instances --region ${region} ${ami} -k ${KEY_NAME} -s ${subnet} -n ${count} -t ${INSTANCE_TYPE}'"

        # Give some time to cancel..
        _dally;

        res=$(ec2-run-instances --region ${region} ${ami} -k ${KEY_NAME} -s ${subnet} -n ${count} -t ${INSTANCE_TYPE})

    done
}


function authorize_ssh() {
    local ip=$1
    if [[ -z "$ip" ]]; then
        echo -e "Need an IP to authorize!"
        exit;
    fi

    echo -e "\t Authorizing SSH access for source IP ${ip}"

    for region in ${REGIONS}; do

        # Fetch all the security groups called default
        for sg in $(ec2-describe-group --region ${region} | grep default | grep -o -E "sg-\S+" | sort | uniq); do

            echo "Running: 'ec2-authorize --region ${region} ${sg} -P tcp -p 22 -s "${ip}/32"'"

            # Give some time to cancel..
            _dally;

            ec2-authorize --region ${region} ${sg} -P tcp -p 22 -s "${ip}/32"
        done
    done

}


function _dally() {

    echo -e "\nPress any key to confirm (or CTRL^C to cancel) .."
    read ak
}

#
#  Starts here
#

case "$1" in
        list)
            list;
            exit 0;
        ;;
        list-ips)
            list ips;
            exit 0;
        ;;
        start)
            start_all "$@";
            exit 0;
        ;;
        stop)
            stop_all "$@";
            exit 0;
        ;;
        create)
            create "$@";
            exit 0;
        ;;
        authorize)
            authorize_ssh "$2";
            exit 0;
        ;;
        *)
            usage;
            exit 1;
        ;;
esac
