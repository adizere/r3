package main

import (
	"errors"

	"github.com/golang/glog"

	"google.golang.org/grpc"
	"google.golang.org/grpc/transport"
)

func extractRemoteAddress(s grpc.Stream) (addr string, err error) {
	ctx := s.Context()
	trs, ok := transport.StreamFromContext(ctx)
	if ok {
		addr = trs.ServerTransport().RemoteAddr().String()
		err = nil
	} else {
		glog.Warningf("Unable to extract the transport from context!")
		addr = ""
		err = errors.New("Invalid transport")
	}
	return addr, err
}
