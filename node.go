package main

import (
	"sync"
	"time"
	"bytes"
	"bufio"
	"math/rand"

	"github.com/golang/glog"
)

type R3Node struct {
	// Helper members
	lastSN uint64 // Counter to generate sequence numbers
	lastTS uint64 // Timestamp to generate entry identifiers
	nodeID int    // Unique identifier for the node holding this log;
	// derived from the node's initial position in ring;
	// this also helps with generating entry identifiers

	Conf          *nodeConf
	Log           *LocalLog
	Network       *Network
	ClientService *ClientService

	MainSuccessor int // Identifier of the main successor; may change
	// in time, e.g., if initial main successor fails
	Successors []SuccessorRef

	reconfigThreshold       int            //This is the threshold for the node to start the reconfiguration, i.e., f+1 in our system
	newconfigThreshold      int            //This is the threshold for the new sequencer to start sending new configuration message, i.e., 4f+1 in our system.
	ReConfigNetworks        []SuccessorRef //This is networks for reconfiguration, they will establish only the reconfiguration start.
	reconfigAlreadyStarted  bool           //Boolean variable to check if reconfiguration is already started.
	newconfigAlreadyStarted bool           //Boolean variable to check if new configuration is already started.

	reconfigStartTimer *time.Timer //This timer will start when replica receives data message, and expects the sequence message.
	//:TODO find a way to handle map timer with data.
	reconfigStartDuration time.Duration
	reconfigSendTicker    *time.Ticker //This ticker is for stalling reconfiguration.
	reconfigSendDuration  time.Duration
	reconfigFlags         map[int32]struct{} //This is for observing the replicas who already start the reconfiguration.

	fireReconfig bool //Hard-code to fire reconfiguration

	// TODO: Move the state vector in its own type

	// The state vector of this node
	// First, the upper limit of the state vector
	sv     map[int]uint64
	svLock sync.RWMutex
	// Second, keep the holes
	svh []map[uint64]struct{}

	cn int32 //this is the view-version identifier
}

func NewNode() *R3Node {

	conf := getNodeConf()

	rand.Seed(int64(conf.Position))

	log := NewLog(4*conf.FaultThreshold + 1)
	net := NewNetwork(conf, log)
	cls := NewClientService()

	node := &R3Node{
		lastSN:                  0,
		lastTS:                  0,
		nodeID:                  conf.Position,
		Conf:                    conf,
		Log:                     log,
		Network:                 net,
		ClientService:           cls,
		Successors:              make([]SuccessorRef, len(conf.Successors)),
		ReConfigNetworks:        make([]SuccessorRef, len(conf.ReConfigNetworks)),
		sv:                      make(map[int]uint64),
		svh:                     make([]map[uint64]struct{}, conf.SystemSize+1),
		cn:                      1,
		reconfigThreshold:       conf.FaultThreshold + 1,
		newconfigThreshold:      4*conf.FaultThreshold + 1,
		reconfigAlreadyStarted:  false,
		newconfigAlreadyStarted: false,
		reconfigSendDuration:    time.Second * 20,
		reconfigStartDuration:   time.Second * 20,
		reconfigFlags:           make(map[int32]struct{}),
		fireReconfig:            conf.StartReconfig,
	}

	// Set each successor..
	for i := range conf.Successors {
		glog.Infof("Setting successor %s", conf.Successors[i])
		node.Successors[i] = *buildSuccessor(conf.Successors[i])
	}

	for i := range conf.ReConfigNetworks { //These are networks for reconfiguration
		glog.Infof("Setting networks for reconfiguration %s", conf.ReConfigNetworks[i])
		node.ReConfigNetworks[i] = *buildSuccessor(conf.ReConfigNetworks[i])
	}

	node.MainSuccessor = node.Successors[0].Position

	// Initialize the state-vector-holes data structure
	// To be consistent with node ID (1..system size), we start from 1
	for i := 1; i <= conf.SystemSize; i++ {
		node.svh[i] = make(map[uint64]struct{})
	}

	// The *log structure might need to call functions on the *node
	log.SetNode(node)

	// The client service implements the client-side HTTP API
	cls.SetNode(node)

	// The networking service implements node to node communication logic
	net.SetNode(node)
	net.Start()

	return node
}

func (n *R3Node) Start() {
	go n.ClientService.Run() // This is a blocking method
	go n.StatusSend()        // Periodically send SV on fallback paths
}

//
// Broadcasts multiple 250-byte LogEntry, each having a dummy body
func (n *R3Node) BroadcastDummyMessages(num int) {
	var sbuf []*Sequence = nil

	// Data msgs buffer
	dbuf := make([]*Data, 0, num)

	for i := 0; i < num; i++ {
		dm := getDataMsg(n.generateID(), bytes.Repeat([]byte("x"), 250))
		dbuf = append(dbuf, dm)

		// Handle this data message as if it were delivered
		sm, _ := n.handleData(dm)
		if sm != nil {
			if sbuf == nil {
				// Sequence msgs buffer
				sbuf = make([]*Sequence, 0, num)
			}
			sbuf = append(sbuf, sm)
		}
	}

	m := assemblePushMsg(dbuf, sbuf, NewNonce())
	glog.Info("Resulting push: %v", m)

	n.broadcastMsg(m)
}

// Handles requests coming through the Client Service (the HTTP API)
func (n *R3Node) Broadcast(s *bufio.Scanner) <-chan bool {
	var firstID string = ""
	var sbuf []*Sequence = nil

	glog.Error("This implementation is naive. Use the other Broadcast() func.")
	panic("Should not be using this function (deprecated)!")

	// Data msgs buffer
	dbuf := make([]*Data, 0, 10)

	for s.Scan() {
		// Create the data message for this request
		dm := getDataMsg(n.generateID(), s.Bytes())
		dbuf = append(dbuf, dm)

		// Handle this data message as if it were delivered
		sm, _ := n.handleData(dm)
		if sm != nil {
			if sbuf == nil {
				// Sequence msgs buffer
				sbuf = make([]*Sequence, 0, 10)
			}
			sbuf = append(sbuf, sm)
		}

		// We'll request to be notified when the entry with this id becomes
		// stable
		if firstID == "" {
			firstID = dm.GetId()
		}
	}
	if err := s.Err(); err != nil {
		glog.Warningf("Error scanning the broadcast request! (%s)", err)
		return nil
	}

	m := assemblePushMsg(dbuf, sbuf, NewNonce())

	n.broadcastMsg(m)

	// Returns a channel.
	// The log will send an event on this channel as soon as this new entry
	// becomes stable.
	return n.Log.NewNotificationRequest(firstID)
}

func (n *R3Node) BroadcastBatch(body []byte) <-chan bool {
	var sbuf []*Sequence = nil

	dm := getDataMsg(n.generateID(), body)
	// Handle this data message as if it were delivered
	sm, _ := n.handleData(dm)
	if sm != nil {
		sbuf = []*Sequence{sm}
	}

	glog.Infof("Broadcasting a batch of len %d", len(body))

	m := assemblePushMsg([]*Data{dm}, sbuf, NewNonce())

	n.broadcastMsg(m)

	// Returns a channel.
	// The log will send an event on this channel as soon as this new entry
	// becomes stable.
	return n.Log.NewNotificationRequest(dm.GetId())
}

func (n *R3Node) broadcastMsg(m *Msg) {
	n.Network.addOutstandingMsg(m)
}

func (node *R3Node) BecomeSequencer(startingSN uint64) {
	panic("Deprecated method.")
	// node.Conf.IsSequencer = true
	// node.lastSN = startingSN
	// glog.Warningf(" >> Becoming a sequencer from SN: %d", startingSN)
}

func (node *R3Node) StatusSend() {
	tck := time.Tick(statusIV * time.Second)

	// Buffer for the latest status vector
	cursv := make([]uint64, node.Conf.SystemSize, node.Conf.SystemSize)

	for {
		select {
		case <-tck:
			node.svLock.RLock()
			// Copy the vector
			for i, e := range node.sv {
				cursv[i-1] = e
			}
			node.svLock.RUnlock()

			node.Network.SendStatusVector(cursv)
		}
	}
}

func (node *R3Node) ReconfigSend() {
	node.reconfigAlreadyStarted = true
	go node.Network.SendReConfig(0)
}

func (node *R3Node) NewConfigSend() {
	node.newconfigAlreadyStarted = true
	node.cn++
	go node.Network.SendNewConfig(0)
}

func (node *R3Node) IsSequencer() bool {
	//return node.Conf.IsSequencer
	return int(node.cn)%node.Conf.SystemSize == node.Conf.Position ||
		(int(node.cn)%node.Conf.SystemSize == 0 && node.Conf.Position == node.Conf.SystemSize)  // handle the case that node on the last position becomes the sequencer.
}

func (node *R3Node) predecessorIsSequencer() bool {
	return int(node.cn+1)%node.Conf.SystemSize == node.Conf.Position
}

func (node *R3Node) ShouldStartReconfig() bool {
	return node.Conf.StartReconfig
}

func (node *R3Node) ActivateFallbackPath() {

}

func (node *R3Node) ReconfigAlreadyStarted() bool {
	return node.reconfigAlreadyStarted
}

func (node *R3Node) NewConfigAlreadyStarted() bool {
	return node.newconfigAlreadyStarted
}

func (node *R3Node) StopReconfig() {
	if node.reconfigSendTicker != nil && node.ReconfigAlreadyStarted() {
		node.reconfigSendTicker.Stop()
		glog.Infof("[reconfig]: Reconfig Timer is stopped")
		node.reconfigAlreadyStarted = false
		glog.Infof("[reconfig]: Reconfiguration is finished")
		for _, v := range node.Log.pendingSet {
			v.Votes = make(map[int]struct{})
		}
	}
}

func (node *R3Node) StopNewConfig() {
	node.newconfigAlreadyStarted = false
	node.reconfigFlags = make(map[int32]struct{})
	node.Log.gatherSet = make(map[int32]map[string]*Sequence)
	glog.Infof("[newconfig]: New Configuration is finished")
}

//assemble pendingset for reconfiguration
func (node *R3Node) generateSMFromPendingSet() *PendingSMSet {
	var pendingSMSet PendingSMSet
	smSet := make(map[string]*Sequence)
	node.Log.pendingLock.Lock()
	defer node.Log.pendingLock.Unlock()
	for k, v := range node.Log.pendingSet {
		sm := getSequenceMsg(k, node.generateID(), v.Header.SN, v.Hash, node.cn)
		smSet[k] = sm
	}
	pendingSMSet.PendingSM = smSet
	return &pendingSMSet
}

func (node *R3Node) assembleReconfig() *ReConfig {
	var reconfig ReConfig
	reconfig.Node = int32(node.nodeID)
	reconfig.Cn = node.cn
	reconfig.PendingSMSet = node.generateSMFromPendingSet()
	return &reconfig
}

func (node *R3Node) assembleReconfigMsg() *Msg {
	var m Msg
	m.Type = &Msg_Reconfig{Reconfig: node.assembleReconfig()}
	return &m
}

func (node *R3Node) assembleNewConfig() *NewConfig {
	var newConfig NewConfig
	newConfig.Cn = node.cn
	newConfig.Rset = node.assembleGatherSMSet()
	return &newConfig
}

func (node *R3Node) assembleNewConfigMsg() *Msg {
	var m Msg
	m.Type = &Msg_Newconfig{Newconfig: node.assembleNewConfig()}
	return &m
}

func (node *R3Node) assembleGatherSMSet() map[int32]*PendingSMSet {
	assSet := make(map[int32]*PendingSMSet)
	node.Log.reprocessLock.Lock()
	defer node.Log.reprocessLock.Unlock()
	for k, v := range node.Log.gatherSet {
		var pendingSMSet PendingSMSet
		pendingSMSet.PendingSM = v
		assSet[k] = &pendingSMSet
	}
	return assSet
}

func (node *R3Node) shouldFireReconfig() bool {
	return node.fireReconfig
}
