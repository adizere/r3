#!/bin/bash

## This is the top-level stressor script.
# Executes consecutive runs of the stressor.sh benchmark


SCRIPTNAME=$0

# Handle the transfer from additional-replicas.txt to replicas.txt
arpsfile="conf/additional-replicas.txt"
rpfile="conf/replicas.txt"

if [[ ! -s ${rpfile} ]]; then
    echo "Cannot continue: the replicas file (${rpfile}) is empty!"
    exit 1;
fi

rcount=`wc -l ${rpfile} | awk '{print $1}'`
echo "Running with ${rcount} replicas"

# Iterate through different parameters for the stressor workload
for i in "160" "180" "200"; do
    echo "    |-- ${i}.." ;

    # Invoke the stressor workload with some particular parameters
    bash "./evaluation/stressor.sh" ${i} ;

    sleep 2;
done;


# Check if we should add additional replicas
if [[ -s "${arpsfile}" ]]; then

    echo "Found additional replicas"

    # Transfer 4 replicas to the original replicas file
    head -n4 ${arpsfile} >> ${rpfile}
    echo "File ${rpfile} extended to: " `wc -l ${rpfile}`

    # Remove the 4 replicas from the "additional" file
    # use awk to capture everything starting from line 4 onwards (first 4
    # lines were transferred to the replicas file)
    tmpf=$(mktemp)
    awk '{if (NR > 4) {print $1}}' ${arpsfile} > ${tmpf}
    mv ${tmpf} ${arpsfile}
    echo "File ${arpsfile} shrinked to: " `wc -l ${arpsfile}`

    echo "Another turn.... ${SCRIPTNAME}"

exec bash "${SCRIPTNAME}"
fi

exit;
