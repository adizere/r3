#!/bin/bash

# Start, stop, and deploy the codebase for a cluster of R3 replicas

SSH_KEY="~/.ssh/r3key"
SSH_OPTS="-o StrictHostKeyChecking=no -i ${SSH_KEY} -l root"
NODES_DEFAULT="conf/replicas.txt"
ENABLE_ROOT_SCRIPT="evaluation/common/enable-root-ssh.sh"

R3_LOCAL_PATH="/root/r3"
R3_REMOTE_PATH="/root/r3"
CFG_DESTINATION="/root/r3/conf/r3-node.json"
INIT_REMOTE_PATH="/root/r3/conf/r3.conf"
R3_PORT=55467


export SSH_KEY
export R3_PORT
export SSH_OPTS
export R3_REMOTE_PATH
export INIT_REMOTE_PATH
export CFG_DESTINATION



function usage()
{
    echo -e "\nUsage:\n\t`basename "$0"` deploy|start|stop|start-local|stop-local" >&2
    echo -e "\t- options deploy|start|stop require additional argument <replica-nodes>=${NODES_DEFAULT}" >&2
    echo -e "\t- option start-local requires an additional argument <replica-count>" >&2
}


function _auth_prereq() {
    bash "${ENABLE_ROOT_SCRIPT}" "${nodes}"
}


# Echoes the successor for a given node in a given file
function _find_successor() {
    local host=$1
    local pos=$2
    local input_file=$3

    successor=$(grep -A1 "${host}$" ${input_file} | awk 'NR==2 {print;}')
    if [[ -z ${successor} ]]; then
        # This is the last node in the list, the successor is the first node
        successor=$(head -n1 ${input_file})
        successorPos="1"
    else
        successorPos=$((${pos}+1))
    fi

    echo "${successor} ${successorPos}"
}


function _generate_node_config()
{
    local input_nodes=$1
    local host=$2
    local pos=$3
    local count=$4  # How many successors should the config include
    local net_num=$5 # How many networks should the config include

    echo "Node $host ring configuration: position = ${pos}" >&2

    # generate the configuration
    succPos=${pos}
    succ=${host}

    successors='["'
    for (( i = 0; i < ${count}; i++ )); do
        if [[ ${i} -gt 0 ]]; then
            successors="${successors},\""
        fi
        succRaw=$(_find_successor "${succ}" "${succPos}" "${input_nodes}")
        succ=$(echo ${succRaw} | awk '{print $1}')
        succPos=$(echo ${succRaw} | awk '{print $2}')

        echo -e "\t - successor ${i}: ${succ} @ ${succPos}" >&2

        # Decide if we add the port or if it's already added
        if [[ "${succ}" =~ \:[0-9]+ ]]; then
            successors="${successors}${succPos},${succ}\""
        else
            successors="${successors}${succPos},${succ}:${R3_PORT}\""
        fi
    done
    successors="${successors}]"

    networks='["'
    for (( i = ${count}; i < ${net_num}; i++ )); do
        if [[ ${i} -gt ${count} ]]; then
            networks="${networks},\""
        fi
        succRaw=$(_find_successor "${succ}" "${succPos}" "${input_nodes}")
        succ=$(echo ${succRaw} | awk '{print $1}')
        succPos=$(echo ${succRaw} | awk '{print $2}')

        echo -e "\t - network ${i}: ${succ} @ ${succPos}" >&2

        # Decide if we add the port or if it's already added
        if [[ "${succ}" =~ \:[0-9]+ ]]; then
            networks="${networks}${succPos},${succ}\""
        else
            networks="${networks}${succPos},${succ}:${R3_PORT}\""
        fi
    done
    networks="${networks}]"



    if [[ ${pos} == "1" ]]; then
        bcast='\n\t"Broadcast": true,'   # Should the node start broadcasting?
        isSeq='\n\t"IsSequencer": true,' # Is this the sequencer?
    else
        bcast='\n\t"Broadcast": false,'
        isSeq='\n\t"IsSequencer": false,'
    fi

    if [ ${pos} == "4" ]; then  # This is a hard-code boolean variable to fire the reconfiguration.
        reConfig='\n\t"StartReconfig": true,'
    else
        reConfig='\n\t"StartReconfig": false,'
    fi

    if [[ "${host}" =~ \:[0-9]+ ]]; then
        host="\"${host}\""
    else
        host="\":${R3_PORT}\""
    fi

    sysize='\n\t"SystemSize": '$(awk 'END{print NR}' ${input_nodes})','

    echo -e "{\n\t\"Position\": ${pos},${sysize}${bcast}${isSeq}${reConfig}\n\
\t\"Listen\": ${host},\n\t\"Successors\": ${successors}, \n\t\"ReConfigNetworks\": ${networks}\n}"
}


function _deploy_node_config()
{
    local input_nodes=$1
    local host=$2
    local pos=$3

    # detect the fault threshold
    nreps=$(awk 'END{print NR}' ${input_nodes})
    nfaults=$((${nreps} / 5))
    if [[ ${nfaults} -lt 1 ]]; then
        nfaults=1
    fi
    nconns=$((${nfaults} + 1))

    echo "Node: ${pos} ~ ${host}"

    config=$(_generate_node_config "${input_nodes}" "${host}" "${pos}" "${nconns}" "${nreps}-1")

    echo "${config}" | ssh ${SSH_OPTS} ${host}\
        "dd of='${CFG_DESTINATION}'";

    return ;
}


#
# This is a sub-procedure of the 'deploy' target
function _deploy_service()
{
    local host=$1
    local archive=$2

    local remdest='/tmp/r3.tar.gz'

    if [[ ! -r ${archive} ]]; then
        echo "Could not deploy the service; source file not found!"
        return
    fi

    echo -e "\t ** Copying the codebase ${archive} on ${host}"
    cat ${archive} | ssh ${SSH_OPTS} "${host}" "dd of='${remdest}'";

    # Extract the codebase archive into /tmp/r3,
    # then move it to its proper place
    echo -e "\t ** Extracting the codebase on ${host}"
    ssh ${SSH_OPTS} "${host}" /bin/bash <<EOT
        tar zxf ${remdest} -C /tmp/
        [ -d ${R3_REMOTE_PATH} ] && rm -rf ${R3_REMOTE_PATH};
        mv /tmp/r3 ${R3_REMOTE_PATH};
        cp ${INIT_REMOTE_PATH} /etc/init/;
EOT

    # Compile the codebase
    echo -e "\t ** Compiling on ${host}"
    ssh ${SSH_OPTS} "${host}" /bin/bash <<EOT
        cd ${R3_REMOTE_PATH};
        protoc --go_out=plugins=grpc:. *.proto
        go get;
        go build;
EOT

    return ;
}


function deploy_cluster()
{
    stop_cluster;

    # Create a local copy of the whole codebase
    # We'll ship this to every node using _deploy_service
    archive="/tmp/r3-archive.tar.gz"
    tar czf ${archive} -C $(dirname ${R3_LOCAL_PATH}) \
        ./`basename ${R3_LOCAL_PATH}` 2>/dev/null
    # Deploy the whole codebase
    export -f _deploy_service
    parallel -j30 -a ${nodes} _deploy_service "{}" "${archive}";

    rm "${archive}"
    deploy_conf;
}

function deploy_conf(){
    # Now deploy the configuration file
    export -f _deploy_node_config
    export -f _find_successor
    export -f _generate_node_config
    parallel -j30 -a ${nodes} _deploy_node_config "${nodes}" "{}" "{#}";

    echo -e "\n\t ** Deployed the cluster config on all replicas\n";

    exit;
}


function stop_cluster()
{
    parallel -j30 -a ${nodes} ssh ${SSH_OPTS} -T "{}" "initctl stop r3";

    echo -e "\n\t.. R3 stopped";
}


function start_cluster()
{
    parallel -j30 -a ${nodes} ssh ${SSH_OPTS} -T "{}" "initctl start r3";

    echo -e "\n\t.. R3 started";
}


function stop_local_cluster()
{
    pkill -9 "r3"
}


function start_local_cluster()
{
    local cnt=$1
    if [[ ! ${cnt} =~ ^[0-9]+$ ]]; then
        echo "How many replicas should we start?"
        exit 1;
    fi
    echo "Starting ${cnt} replicas locally."

    # Generate the replicas description file
    treps=$(mktemp)
    for i in `seq ${cnt}`; do
        port=$((45455 + ${i}))
        echo "127.0.0.1:${port}" >> ${treps} ;
    done
    echo -e "\t temporary replicas description .. ${treps}"

    # detect the fault threshold
    nfaults=$((${cnt} / 5))
    if [[ ${nfaults} -lt 1 ]]; then
        nfaults=1
    fi
    nconns=$((${nfaults} + 1))

    # Generate the configuration file for every replica, and start each replica
    for i in `seq ${cnt}`; do
        port=$((45455 + ${i}))
        host="127.0.0.1:${port}"
        cfg=$(_generate_node_config "${treps}" "${host}" "${i}" "${nconns}" "${cnt}-1")
        cfgname="conf/local-conf-${i}.json"
        echo ${cfg} > ${cfgname}

        # Start..
        logname="local-${i}.log"
        nohup ./r3 -logtostderr=true -cfg ${cfgname} >${logname} 2>&1 &

        echo -e "\t started ${i}"
    done

    rm ${treps};
}


function fetch_logs()
{
    local destdir=$1
    # Count the dir contents; only 1 (the dir itself) should be found.
    local cnt=$(find ${destdir} -printf '.' 2>/dev/null | wc -c)
    # Check that the directory exists and it is empty.
    if [ ! -d ${destdir} ] || [ ! ${cnt} == "1" ]; then
        echo "! Fatal: Need an **empty** destination directory for logs!"
        usage;
        return
    fi

    echo -e "\n[Copying service logs from all replicas]"

    export -f _fetch_replica_log
    parallel -j30 -C"\s+" -a ${nodes} _fetch_replica_log \
        {#} {1} ${destdir};

    # Create an archive of all the logs.
    arcname="$(dirname ${destdir})/$(basename ${destdir}).tar.gz"
    tar czf ${arcname} -C $(dirname ${destdir}) ${destdir}/* 2>/dev/null

    # Can now remove the raw log data, otherwise the logs will quickly fill up
    # the disk.
    # rm -r ${destdir}/*

    echo -e "\n\tFinished fetching service logs to .. \
        $(realpath --relative-base=. ${destdir})"
    echo -e "\tLogs zipped-up in .. \
        $(realpath --relative-base=. ${arcname})"
}


function _fetch_replica_log()
{
    local rid=$1            # Replica's ID.
    local raddr=$2          # Replica IP.
    local dest=$3           # Destionation directory where we'll store the log.

    local remln="/tmp/rep${rid}.log"          # Remote name of the log.
    local remlgz="/tmp/rep${rid}.tar.gz"      # Remote name of the log archive.
    local localln="${dest}/rep${rid}.tar.gz"  # Local name of the log.
    echo -n "rep${rid} .. "

    # Create the archive with this replica's log.
    # In case the replica does not have a log, then create (or overwrite any
    # previous) archive of the log.
    ssh ${SSH_OPTS} ${raddr} -T "cp /var/log/r3.log ${remln};
        tar czf ${remlgz} -C /tmp/ ${remln} 2>&1" >/dev/null

    # Now copy the archive.
    scp root@${raddr}:${remlgz} ${localln} >/dev/null

    # Extract the archive
    tar zxvf ${localln} -C ${dest}
    rm ${localln}
}


#
# Starts here
#

if [[ -s "$2" ]]; then
    nodes=$2
else
    nodes=${NODES_DEFAULT}
fi

if [[ ! -r "${nodes}" ]]; then
    echo "Input '${nodes}' not valid: need a file with all the replicas of the cluster!";
    usage;
    exit;
fi



echo -e "Settings:"
echo -e "\t Replicas file: .... ${nodes}"


# Ensure that root login is possible on all target nodes
_auth_prereq;


case "$1" in
        deploy)
            deploy_cluster;
            exit 0;
        ;;
        deploy-conf)
            deploy_conf;
            exit 0;
        ;;
        start)
            start_cluster;
            exit 0;
        ;;
        stop)
            stop_cluster;
            exit 0;
        ;;
        stop-local)
            stop_local_cluster;
            exit 0;
        ;;
        start-local)
            start_local_cluster $2;
            exit 0;
        ;;
        fetch-logs)
            fetch_logs $3;
            exit 0;
        ;;
        *)
            usage;
            exit 1;
        ;;
esac
