import requests, threading, sys, time, os, multiprocessing, ctypes, random
import numpy as np
from multiprocessing import Pool, Manager, Process


current_milli_time = lambda: int(round(time.time() * 1000))

# How many seconds should the execution last?
EXECUTION_DURATION=10

# How many seconds should we wait for connections initialization
CONNNECTION_SLACK=2

# How many seconds should we wait for outlying executors
EXECUTION_SLACK=3

# Transaction timeout, in seconds
TX_TIMEOUT=10

# How many messages per request
MESSAGES_COUNT=6

class R3Fabric(object):
    def __init__(self, ip, port):
        # Sanitize input
        address = str(ip).strip() + ":" + str(port).strip()
        print "Executor initializing -- " + str(os.getpid()) + " to " + address
        self.service = 'http://' + address + "/tx/"

    # Does a transaction
    def tx(self, txPayload):
        try:
            r = requests.put(self.service, data=txPayload, timeout=TX_TIMEOUT)
        except requests.exceptions.Timeout, e:
            print "Timeout while executing TX: " + str(e)
            return 0
        else:
            if (r.status_code != 202):
                print "Error occurred: TX not accepted" + str(r)
                return 0
            else:
                return 1


class WorkloadResults(object):
    def __init__(self,contactNodeIP="",counter = 0,latencies = []):
        self.opCounter = counter
        self.opLatencies = latencies
        self.runtime = 0
        self.contactIP = contactNodeIP

    def getCounter(self):
        return self.opCounter

    def getLatencies(self):
        return self.opLatencies

    def merge(self, anotherWorkloadRes):
        self.opCounter += anotherWorkloadRes.getCounter()
        self.opLatencies += anotherWorkloadRes.getLatencies()

    def setRuntime(self, time):
        self.runtime = time

    def printStatistics(self):
        if (self.opCounter > 0):
            # avoid division by 0
            throughput = self.opCounter / (float(self.runtime) / 1000)
        else:
            # print 0 if the counter is 0..
            throughput = 0
        print("%10dops\t%10dms\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%s" % (
            self.opCounter,
            self.runtime,
            throughput,
            np.mean(self.opLatencies),
            np.std(self.opLatencies),
            np.percentile(self.opLatencies, 95),
            np.percentile(self.opLatencies, 99),
            self.contactIP))



def execute_workload(ip, port, blockSize, startEv, stopEv, pLock, pCounter):
    r3 = R3Fabric(ip, port)
    print "Executor waiting -- " + str(os.getpid())
    startEv.wait()
    print "Executor running -- " + str(os.getpid())
    ## TODO: Perhaps use a random string instead of hardcoded 'x'
    item = "x" * blockSize + "\n"
    payload = item * MESSAGES_COUNT
    cnt = 0
    lats = []
    while stopEv.is_set() == False:
        startt = current_milli_time()
        res = r3.tx(payload)   # blocks until the request finishes
        if res == 1:        # success
            lats.append(current_milli_time() - startt)
            cnt += MESSAGES_COUNT
            pLock.acquire()
            pCounter.value += MESSAGES_COUNT
            pLock.release()
    print "Executor finished " + str(cnt) + " -- " + str(os.getpid())
    return WorkloadResults(counter=cnt, latencies=lats)


def periodic_reports(pLock, pCounter):
    stamp = 0
    while True:
        pLock.acquire()
        print str(stamp) + " > " + str(pCounter.value)
        pCounter.value = 0
        pLock.release()
        stamp += 1
        time.sleep(1)

def parse_ip(target):
    if (os.path.isfile(target)):
        return random.choice(list(open(target)))
    else:
        return target

if __name__ == '__main__':
    with Manager() as manager:
        startEvent = manager.Event()
        stopEvent = manager.Event()
        pLock = manager.RLock()
        pCounter = manager.Value('i', 0)
        executorsCount = int(sys.argv[4])
        print "Using a pool of " + str(executorsCount) + " executors"
        pool = Pool(processes=executorsCount)
        executors = [
            pool.apply_async(
                    execute_workload,
                    [parse_ip(sys.argv[1]),     # IP
                    int(sys.argv[2]),           # port
                    int(sys.argv[3]),           # block size
                    startEvent, stopEvent,
                    pLock, pCounter])
            for x in xrange(executorsCount)]
        PeriodicReporter = Process(target=periodic_reports, args=(pLock, pCounter))
        PeriodicReporter.start()
        # Allow executors to establish their connections
        print "Sleeping for " + str(CONNNECTION_SLACK) + " second(s) to establish connections.."
        time.sleep(CONNNECTION_SLACK)
        start_time = current_milli_time()
        startEvent.set()    # start executors
        time.sleep(EXECUTION_DURATION)
        stopEvent.set()     # stop executors
        wkResult = WorkloadResults(contactNodeIP=sys.argv[1])
        delta = current_milli_time() - start_time
        time.sleep(EXECUTION_SLACK)
        PeriodicReporter.terminate()
        PeriodicReporter.join()
        for t in executors:
            try:
                wr = t.get(None)
            except:
		e = sys.exc_info()[0]
                print "--Caught it!!" + str(e)
            else: #if wr is not None:
                print "wr: " + str(wr.getCounter())
                wkResult.merge(wr)
        print " --> total: " + str(wkResult.getCounter())
        wkResult.setRuntime(delta)
        wkResult.printStatistics()
