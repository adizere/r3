#!/bin/bash

### Stressor client / worker node
# This is a general-purpose script, should be used with all workloads


LOCAL_REPORT_FILE="/tmp/client-worker-report"
LOCAL_WORKER_NAME="client-worker.sh"
DEFAULT_WORKLOAD="client-workload.py"
HL_WORKLOAD="hl-client-workload"


function usage()
{
    echo -e "\nUsage:\n\t`basename "$0"` start|stop <service-replica-IP>\
    <service-replica-port> <block-size-kB> <process-count>\
    [<workload-name>=${DEFAULT_WORKLOAD}] " >&2
}


function start_local_worker()
{
    report_file="${LOCAL_REPORT_FILE}"

    echo "Using report file: ${report_file}"

    # Now start the workload
    # distinguish between the java-based workload (bfts)
    # and the python-based workloads (zk and etcd)
    if [[ "${workload_fname}" == "${DEFAULT_WORKLOAD}" ]]; then
        # the default workloads are in python
        stdbuf -oL python "./${workload_fname}" "${replica_ip}" "${replica_port}" "${block_size}" "${process_count}" 2>&1 >${report_file} &
    elif [[ "${workload_fname}" == "${HL_WORKLOAD}" ]]; then
        # this is the Hyperledger workload
        stdbuf -oL ./${workload_fname} -addr ${replica_ip}:6100 -listen -parallel "${process_count}" -size "${block_size}" 2>&1 >${report_file} &
    else
        # bfts has a java workload, for the moment just hardcode it
        cd /root/bfts/ && java -cp /root/bfts/bin/*:/root/bfts/lib/* ${workload_fname} "${process_count}" 2>&1 >${report_file} &
    fi

    exit 0;
}


function stop_local_worker()
{
    pkill --signal 9 -of "${LOCAL_WORKER_NAME}" ;
    pkill --signal 9 -f "${workload_fname}" ;
    exit 0;
}


######## Execution starts here
replica_ip=$2
if [[ -z "${replica_ip}" ]]; then
    echo "Need the IP of a serice replica!" >&2;
    usage;
    exit 1;
fi

replica_port=$3
if [[ -z "${replica_port}" ]]; then
    echo "Need the service replica port!" >&2;
    usage;
    exit 1;
fi


block_size=$4
if [[ -z "${block_size}" ]]; then
    echo "Need the block size!" >&2;
    usage;
    exit 1;
fi

process_count=$5
if [[ -z "${process_count}" ]]; then
    echo "Need the process_count!" >&2;
    usage;
    exit 1;
fi

workload_fname=$6
if [[ -z "${workload_fname}" ]]; then
    workload_fname=${DEFAULT_WORKLOAD}
fi

case "$1" in
        start)
            start_local_worker;
            exit 0;
        ;;
        stop)
            stop_local_worker;
            exit 0;
        ;;
        *)
            usage;
            exit 1;
        ;;
esac
