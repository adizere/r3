#!/bin/bash

### Stressor client / master node
# Keeps connections with client worker nodes and sends commands,
# e.g., start and stop


# High-level interaction:
#
# client-master
#     |-----start|stop-------|--> client-worker <---> client-workload
#                            |--> client-worker <---> client-workload
#                            |--> client-worker <---> client-workload
#                                          ...
#                            |--> client-worker <---> client-workload
#

DEFAULT_WORKERS_FILE="conf/workers.txt"

SSH_KEY="~/.ssh/r3key"
SSH_OPTS="-o StrictHostKeyChecking=no -i ${SSH_KEY}"

WORKER_ROOT="/root/stressor-client-worker"
WORKER_SCRIPT="evaluation/clients/client-worker.sh"
DEFAULT_WORKLOAD_SCRIPT="evaluation/clients/client-workload.py"
NODES_DEFAULT="conf/replicas.txt"

DEFAULT_BLOCK_SIZE=250
DEFAULT_EXECUTORS_COUNT=1

# export these to be able to access them from `parallel` jobs
export SSH_OPTS
export SSH_KEY
export WORKER_SCRIPT
export WORKER_ROOT
export NODES_DEFAULT


function usage()
{
    echo -e "\nUsage:\n\t`basename "$0"` deploy|start|stop <replica-IP> \
<replica-port> [<executors-per-worker>=${DEFAULT_EXECUTORS_COUNT}] \
[<block-size-kB>=${DEFAULT_BLOCK_SIZE}] \
[<workers-file>=${DEFAULT_WORKERS_FILE}] \
[<workload-script>=${DEFAULT_WORKLOAD_SCRIPT}] " >&2
}


function _scp_client_files()
{
    local wip=$1       # worker's IP
    local wscript=$2   # the path to the workload script, which has to be copied

    scp ${SSH_OPTS} ${WORKER_SCRIPT} ${wscript} ${NODES_DEFAULT} \
        "root@${wip}:${WORKER_ROOT}" >/dev/null

    echo -en "\t\t |- ${wip} .. done\n";
}


function deploy_workers()
{
    echo -e "\t * Deploying client-worker script on every worker node.."

    # make sure the scripts to deploy are available to us
    for script in ${WORKER_SCRIPT} ${workload_script} ${NODES_DEFAULT} ; do
        if [[ ! -e ${script} ]]; then
            echo -e "*** Could not locate the requisite script at ${script}"
            exit 1;
        fi
    done

    # make sure the destination directory exists
    parallel -j30 -a ${input} ssh ${SSH_OPTS} -i ${SSH_KEY} -T "root@{}"\
        "mkdir -p ${WORKER_ROOT}"

    # now copy the files via scp
    export -f _scp_client_files
    parallel -j30 -a ${input} "_scp_client_files {} ${workload_script}"
}


function start_workers()
{
    _invoke_workers "start";

    exit 0;
}


function stop_workers()
{
    _invoke_workers "stop";
    exit 0;
}


function _invoke_workers()
{
    local cmd=$1
    echo -e "\t * Invoking command '${cmd}' on each worker.."

    for wip in $(awk '{print $NF}' ${input}); do
        echo -en "\t\t |- ${wip} .. ";

        # if we have multiple node options, we'll choose one at random
        narray=(${contact_ip})
        nodes_count=${#narray[@]}
        if [[ "${nodes_count}" -gt 1 ]]; then
            choice=`perl -e "print int(rand(${nodes_count}));"`
            contact_node=${narray[${choice}]}
        else
            contact_node=${contact_ip}
        fi
        echo -n "selected contact node: " ${contact_node} " .. "

	    wkscript=$(basename ${workload_script})

        {
        ssh -i ${SSH_KEY} -T "root@${wip}" "cd ${WORKER_ROOT};
        nohup bash ./client-worker.sh ${cmd} ${contact_node}\
        ${contact_port} ${block_size} ${executors_cnt}\
        ${wkscript} </dev/null >/dev/null 2>&1 &" ||
            {
                echo -e "\n*** Make sure you 'deploy' workers before invoking\
                    an operation on them! ***";
                echo -e "Interrupted ... could not start.";
                exit 1;
            }
        }&

        echo -en "done \n";
    done
}


######## Execution starts here

# This argument is mandatory
contact_ip=$2
if [[ -z "${contact_ip}" ]]; then
    echo "Need the IP & port of a service replica, preferably the master/leader!";
    usage;
    exit 1;
fi

contact_port=$3
if [[ -z "${contact_port}" ]]; then
    echo "Need the contact port of a service replica!";
    usage;
    exit 1;
fi

if [[ -z $4 ]]; then
    executors_cnt=${DEFAULT_EXECUTORS_COUNT}
else
    executors_cnt=$4
fi

if [[ -z $5 ]]; then
    block_size=${DEFAULT_BLOCK_SIZE}
else
    block_size=$5
fi

# we need the file with IP addresses of all workers
if [[ ! -r "$6" ]]; then
    input=${DEFAULT_WORKERS_FILE}
else
    input=$6
fi
if [[ ! -r "${input}" ]]; then
    echo "Need a file with the IPs of all the client workers!";
    usage;
    exit 1;
fi

if [[ -z "$7" ]]; then
    workload_script=${DEFAULT_WORKLOAD_SCRIPT}
else
    workload_script=$7
fi

echo -e "Settings:"
echo -e "\t Service replica IP: ... ${contact_ip}"
echo -e "\t Service port: ......... ${contact_port}"
echo -e "\t Threads per worker: ... ${executors_cnt}"
echo -e "\t Block size: ........... ${block_size}"
echo -e "\t Workers file: ......... ${input}"
echo -e "\t Workload script: ...... ${workload_script}\n\n"


case "$1" in
        deploy)
            deploy_workers;
            exit 0;
        ;;
        start)
            start_workers;
            exit 0;
        ;;
        stop)
            stop_workers;
            exit 0;
        ;;
        *)
            usage;
            exit 1;
        ;;
esac
