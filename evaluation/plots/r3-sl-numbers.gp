reset
set terminal postscript eps size 14,3.5 enhanced color font 'Helvetica,30'
set output "r3-sl-numbers.eps"

set ytics out nomirror offset 0.7 scale 0.9
set xtics out nomirror scale 0.7 offset -0.5,0 rotate by -60


set yrange [0:15500]
set xrange [1:108]

set tics scale 0.5

set style line 1 lt 1 lw 2 pt 7 ps 2 pi -1
set style line 2 lt 1 lw 2 lc rgb '#E41B17' pt 5 ps 2 pi -1
set style line 3 lt 2 lw 2 lc rgb '#0060ad' pt 6 ps 2 pi -1
set pointintervalbox 3

set key right box height 1

set style line 12 lc rgb '#ddccdd' lt 3 lw 1.5
set style line 13 lc rgb '#ddccdd' lt 3 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 13

set xlabel "Carousel system size (number of replicas in the system)" offset 2
set ylabel "Peak throughput\n(tps)" offset 2

set label "12353.94" at screen 0.1,0.92 rotate by -60 font 'Helvetica,26'
set label "8649.77" at screen 0.13,0.58   rotate by -60 font 'Helvetica,26'
set label "4228.01" at screen 0.947,0.397   rotate by -60 font 'Helvetica,26'

set lmargin at screen 0.09
#set rmargin at screen 0.9

plot '../results-fallback' u 1:2:xtic(1) w lp t "Throughput (faulty case)" ls 2,\
    '' u 1:2:3 w yerrorb notitle ls 2,\
    '../results-good' u 1:2 w lp t "Throughput (common case)" ls 3 lw 2,\
    '' u 1:2:3 w yerrorb notitle ls 3,\
    '../results-good' every 2::3 u 1:($2 + 100):($2) with labels rotate by -60 font 'Helvetica,26' offset -1,1.5 notitle,\
    '../results-fallback' every 2::3 u ($1 + 2):($2 - 4500):($2) with labels rotate by -60 font 'Helvetica,26' offset -1,1.5 notitle
