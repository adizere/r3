
## Extracts three columns from a 15-columns .raw file
# The three columns are:
# (1) number of replicas in the ZK ensemble
# (2) the maximum throughput observed for that number of replicas
# (3) the standard deviation for that throughput


function extract_from_file()
{
    local input=$1
    awk '{
        # if the throughput ($5) is the largest observed for this # replicas, remember it
        if ((!($13 in max)) || (($13 in max) && ($5 > max[$13])))
        {
            max[$13] = $5;
            stddev[$13] = $6;
            avglat[$13] = $7;
            latstddev[$13] = $8;
            }
        } END {
            for (i in max)
                if ( i > 0)
                    printf "%d\t%.2f\t%.2f\t%.2f\n",
                        i, max[i], stddev[i], avglat[i];
        }' ${input} | sort -nk 1
}

if [[ ! -d "$1" ]]; then
    echo "Need a directory with .raw file(s)."
    exit 1;
fi
dir=$1

echo -e "\n\n\tUsing directory: .. ${dir}\n" >&2

for raw_file in `find "${dir}" -type f -name "*.raw"`; do
    echo -e "\n\tUsing file: ....... ${raw_file}" >&2
    extract_from_file ${raw_file}
done
