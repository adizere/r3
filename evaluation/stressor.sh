#!/bin/bash

# Stressor script for R3
#
# Coordinates an R3 cluster and a host of clients to obtain throughput/latency
# measurements for the system
#

SSH_KEY="~/.ssh/r3key"
SSH_OPTS="-o StrictHostKeyChecking=no -i ${SSH_KEY} -l root"


CLIENT_WORKERS="conf/workers.txt"
R3_NODES="conf/replicas.txt"

DEFAULT_EXECUTORS_PER_WORKER=10
DEFAULT_BLOCK_SIZE=250                  # 250 bytes
DEFAULT_SERVICE_PORT=8080


RUNS=3
REST_PERIOD=10  # rest period between subsequent runs
STARTUP_SLACK=15 # rest period upon a cluster startup before we bombard it
                # with client requests

# should be the total experiment time (see client-workload.py) + some delay
EXPERIMENT_DURATION=20
TEMP_REPORT_FILE="evaluation/__report.tmp"

## SCRIPTS locations
MASTERSCRIPT="evaluation/clients/client-master.sh"
WORKERSCRIPT="evaluation/clients/client-worker.sh"
CONTROLSCRIPT="evaluation/replicas/r3-control-cluster.sh"

ENABLE_ROOT_SCRIPT="evaluation/common/enable-root-ssh.sh"

function usage()
{
    echo -e "\nUsage:\n\t`basename "$0"` [<executors-per-worker>=${DEFAULT_EXECUTORS_PER_WORKER}] " >&2
}


function restart_cluster()
{
    echo -e "\n** Restarting the R3 cluster\n\n"

    bash ${CONTROLSCRIPT} stop
    echo -e "\n\t** Stop: done\n"

    bash ${CONTROLSCRIPT} start
    echo -e "\n\t** Start: done\n"
}

### STARTS HERE

usage;

if [[ ! -z "$1" ]]; then
    executors_count=$1
else
    executors_count=${DEFAULT_EXECUTORS_PER_WORKER}
fi

echo -e "Settings:"
echo -e "\t -- # of executors per worker ... ${executors_count}\n\n"


# enable root login on clients
# bash "${ENABLE_ROOT_SCRIPT}" "${CLIENT_WORKERS}"

# deploy clients -- a one-time operation
# echo -e "\n** Deploying the clients workers\n\n"
bash ${MASTERSCRIPT} deploy "dummyIP" "dummyPort"


# generate the uuid for this experiment
muid=`uuidgen | cut -c 1-6`
echo -e "\n----\n---- id=${muid} $(date) \n" >> evaluation/results.raw

sum_runs=0
thr_values=""
latencies=""

for (( i = 0; i < ${RUNS}; i++ )); do
    bash ${MASTERSCRIPT} stop "dummyIP" "dummyPort"

    # Restart the cluster to clean-up any data or remainders from
    # previous experiment(s)
    restart_cluster ;

    # Clean the output
    > ${TEMP_REPORT_FILE}

    # Allow the cluster some time to start
    sleep ${STARTUP_SLACK};

    # the IPs of the nodes
    replicas=$(cat ${R3_NODES})
    rcnt=$(wc -l ${R3_NODES} | awk '{print $1}')
    echo "Client workers will connect to a random replica out of ${rcnt}"

    # Start the clients
    bash ${MASTERSCRIPT} start "replicas.txt" ${DEFAULT_SERVICE_PORT} ${executors_count} ${DEFAULT_BLOCK_SIZE} ${CLIENT_WORKERS}

    # Now wait for worker's output
    echo -e "\n** Waiting for output from client workers "
    sleep ${EXPERIMENT_DURATION};

    # Copy the execution results from each worker machine
    while read -u 52 w; do
        res=`ssh ${SSH_OPTS} -T "${w}" "tail -n1 /tmp/client-worker-report"`
        echo "${w} ${res}" >> ${TEMP_REPORT_FILE}

    done 52<${CLIENT_WORKERS};

    cat ${TEMP_REPORT_FILE} >> evaluation/results.raw

    # compute the throughput of this run
    current_result=`awk '
        BEGIN { tot = 0 }
        {
            sum += $4
            if ( $4 ~ /[0-9]+.[0-9]+/)
                tot ++
        }
        END {
            printf "sum throughputs: %.2f (%d / %d) \n\n", sum, tot, NR >>"evaluation/results.raw"
            printf "%.2f", sum
        }' ${TEMP_REPORT_FILE}`
    echo "Current run result: " ${current_result}

    # Keep the sum of throughputs, so we can compute the avg over all runs
    sum_runs=`echo "${sum_runs} + ${current_result}" | bc -l`
    thr_values="${thr_values} ${current_result}"
    # echo "Sum so far: " ${sum_runs}

    # We should also remember the latencies for one of the clients
    # Select a client which connected to the leader (to be consistent across different experiments)
    if [[ -z "${latencies}" ]]; then
        latencies=`head -n1 ${TEMP_REPORT_FILE} | awk '{print $5, $6, $7, $8}'`
        echo "Latencies extracted: " ${latencies}
    fi

    sleep ${REST_PERIOD}
done

avg_throughput=`echo "${sum_runs} / ${RUNS}" | bc -l`
sumsq=0
for v in ${thr_values}; do
    sumsq=`echo "${sumsq} + (${v} - ${avg_throughput})^2" | bc -l`
done

stddev=`echo " sqrt(${sumsq} / (${RUNS} - 1)) " | bc -l`

# How many clients (workers) do we expect to start?
workers_cnt=`wc -l ${CLIENT_WORKERS} | awk '{print $1}'`

# How many replicas did the ensemble comprise?
replicas_cnt=`wc -l ${R3_NODES} | awk '{print $1}'`

printf "%s\t%d\t%d\t%d\t%.2f\t%.2f\t%s\t%d\t%d\t%s\t%s\n" \
    ${muid} \
    ${workers_cnt} \
    ${executors_count} \
    $((${workers_cnt} * ${executors_count})) \
    ${avg_throughput} \
    ${stddev} \
    "${latencies}" \
    ${DEFAULT_BLOCK_SIZE} \
    ${RUNS} \
    ${replicas_cnt} | tee -a evaluation/results
