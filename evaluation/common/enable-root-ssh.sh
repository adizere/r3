#!/bin/bash

##
# This script does two things:
#   1. ssh as user `ubuntu` on a machine
#   2. enables `root` login for that machine
#
# This is especially useful for EC2 machines.
# On these machines, only `ubuntu` can login -- a security measure.
# To permit `root` login we need to modify
# the `/root/.ssh/authorized_keys` file. To do so, we ssh using `ubuntu`, copy
# a script called `clean-authorized-keys.sh`, and run it remotely; this script
# does the job of modifying the `/root/.ssh/authorized_keys` file properly.
#
#
# Notes:
# - Usually, this script won't be executed directly, but from the
# `ec2-prepare.sh` script.
#


SSH_KEY="/root/.ssh/now-key.pem"
AUTH_CLEAN_SCRIPT="evaluation/common/clean-authorized-keys.sh"


##
## This is a sub-procedure of the 'deploy' target
function _check_auth()
{
    local ip=$1
    local skey=$2
    local cscript=$3

    # execut a dummy command over SSH to check whether we can login as root
    can=`ssh -o StrictHostKeyChecking=no -i ${skey} -T "ubuntu@${ip}" "sudo grep 'Please' /root/.ssh/authorized_keys"`
    if [[ ! -z "${can}" ]]; then
        # root is not available ..
        tg="/tmp/clean-authorized-keys.sh"

        echo -e "\t.. Copying the authorized_keys cleanup script to ${ip}"
        echo -e "yes\n" |
            scp -i ${skey} ${cscript} "ubuntu@${ip}:${tg}"

        echo -e "\t.. Cleaning up the authorized_keys file"
        ssh -i ${skey} -T "ubuntu@${ip}" "sudo bash ${tg} ; rm ${tg}"
    fi
    echo "Root login OK for replica" ${ip}
}


function enable_root()
{
    if [[ ! -r "${AUTH_CLEAN_SCRIPT}" ]]; then
        echo "Cannot find the script to clean up the authorized_keys;\
 it should be at ${AUTH_CLEAN_SCRIPT}";
        exit;
    fi

    export -f _check_auth
    parallel -j30 -a ${input} "_check_auth {} ${SSH_KEY} ${AUTH_CLEAN_SCRIPT}";
}


### Starts here

echo -e "\tWARNING: WILL NOT ENABLE ROOT (running on SoftLayer?)" >&2
exit;
# input is a file with IPs of machines, one per line
input=$1

if [[ ! -r "${input}" ]]; then
    echo "error: Need the file with the IPs of target machines!" >&2
    echo -e "\nUsage:\n\t`basename "$0"` <input-file>" >&2
    echo -e "\nExample:\n\t`basename "$0"` conf/replicas.txt" >&2
    exit -1
fi

# ensure that we can ssh as root
enable_root;

exit;
