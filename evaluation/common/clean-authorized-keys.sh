#!/bin/bash

##
## To be executed remotely through SSH on a ZK replica
##

# make a copy of the original
cp /root/.ssh/authorized_keys /tmp/_original_authorized_keys

# clean the original & save in another place
cat /root/.ssh/authorized_keys | sed -rn 's/.*(ssh-rsa .*)/\1/p' > /tmp/_clean_authorized_keys

# now overwrite the original
cp /tmp/_clean_authorized_keys /root/.ssh/authorized_keys
